# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# - supervised-learning
#     - classification
#     - regression
# - unsupervised learning

# %%
from sklearn import datasets
iris = datasets.load_iris()
digits = datasets.load_digits()

# %%
digits.data

# %%
digits.target

# %%
from sklearn import svm
clf = svm.SVC(gamma=0.001, C=100.)

# %%
clf.fit(digits.data[:-1], digits.target[:-1])

# %%
clf.predict(digits.data[-1:])

# %%
