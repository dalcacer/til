# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
# %matplotlib inline
import matplotlib.pyplot as plt

plt.style.use('seaborn')

# %% [markdown]
# **Classification Example**

# %%
# Import the example plot from the figures directory
from fig_code import plot_sgd_separator
plot_sgd_separator()

# %% [markdown]
# **Regression Example**

# %%
from fig_code import plot_linear_regression
plot_linear_regression()

# %% [markdown]
# **scikit-learn Data structure**

# %% [markdown]
# - n_samples
# - n_features
#
# ![](https://nbviewer.jupyter.org/github/jakevdp/sklearn_tutorial/blob/master/notebooks/images/data-layout.png)

# %% [markdown]
# **Dateset Example**

# %%
from IPython.core.display import Image, display
display(Image(filename='images/iris_setosa.jpg'))
print("Iris Setosa\n")

display(Image(filename='images/iris_versicolor.jpg'))
print("Iris Versicolor\n")

display(Image(filename='images/iris_virginica.jpg'))
print("Iris Virginica")

# %%
from sklearn.datasets import load_iris
iris = load_iris()

# %%
iris.keys()

# %%
n_samples, n_features = iris.data.shape
print(n_samples, n_features)
print(iris.data[0])

# %%
print(iris.data.shape)
print(iris.target.shape)

# %%
print(iris.target)

# %%
print(iris.target_names)

# %%
import numpy as np
import matplotlib.pyplot as plt

x_index = 0
y_index = 1

#this formatter will label the colorbar with the correct target names
formatter = plt.FuncFormatter(lambda i, *args: iris.target_names[int(i)])

plt.scatter(iris.data[:,x_index], iris.data[:, y_index], c=iris.target, cmap=plt.get_cmap('RdYlBu', 3))
plt.colorbar(ticks=[0,1,2], format=formatter)
plt.clim(-0.5,2.50)
plt.xlabel(iris.feature_names[x_index])
plt.ylabel(iris.feature_names[y_index])


# %% [markdown]
# **Other Datasets**

# %%
from sklearn import datasets

# %%
datasets.fetch_20newsgroups
