# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
# %matplotlib inline
import matplotlib.pyplot as plt

plt.style.use('seaborn')

# %%
# Import the example plot from the figures directory
from fig_code import plot_sgd_separator
plot_sgd_separator()

# %%
