# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Don't forget

# %% [markdown]
# - https://towardsdatascience.com/python-for-data-science-8-concepts-you-may-have-forgotten-i-did-825966908393
# - https://likegeeks.com/python-list-functions/
# - https://book.pythontips.com/en/latest/

# %% [markdown]
# ## Ternary Operations

# %% [markdown]
# condition_if_true if condition else condition_if_false

# %%
is_nice = True
"nice" if is_nice else "not nice"

# %% [markdown]
# (if_test_is_false, if_test_is_true)[test]

# %%
nice = True
personality = ("mean", "nice")[nice]
print("The cat is ", personality)
# Output: The cat is nice

# %%
output = None
msg = output or "No data returned"
msg

# %% [markdown]
# ## Iterator / Generator / Yield

# %%
mylist = [x*x for x in range(3)]      # Iterator
print(type(mylist))
for i in mylist:
    print(i)

# %%
mygenerator = (x*x for x in range(3)) # Generator in Mem
print(type(mygenerator))
for i in mygenerator:
    print(i)


# %% [markdown]
# **yield** is a keyword that is used like return, except the function will return a generator.

# %%
# generator version
def fibon(n):
    a = b = 1
    for i in range(n):
        yield a
        a, b = b, a + b

for i in fibon(2):
    print(i)


# %%
def f123():
    yield 1
    yield 2
    yield 3

for item in f123():
    print(item)

# %% [markdown]
# ## Sets

# %%
some_list = ['a', 'b', 'c', 'b', 'd', 'm', 'n', 'n']
duplicates = set([x for x in some_list if some_list.count(x) > 1])
print(duplicates)

# %% [markdown]
# **Intersect**

# %%
valid = set(['yellow', 'red', 'blue', 'green', 'black'])
input_set = set(['red', 'brown'])
print(input_set.intersection(valid))

# %% [markdown]
# **Difference**

# %%
valid = set(['yellow', 'red', 'blue', 'green', 'black'])
input_set = set(['red', 'brown'])
print(input_set.difference(valid))


# %% [markdown]
# ## __slots__ to reduce memory consumption

# %% [markdown]
# Do not use **__dict__** but register these two attirbutes for storage.

# %%
class MyClass(object):
    __slots__ = ['name', 'identifier']
    def __init__(self, name, identifier):
        self.name = name
        self.identifier = identifier
        self.set_up()
    # ...


# %% [markdown]
# ## List Comprehension

# %% [markdown]
# `variable = [out_exp for out_exp in input_list if out_exp == 2]`

# %%
x = [1,2,3,4]
out = [item**2 for item in x]
print(out)

# %% [markdown]
# ## Dict Comprehension

# %%
mcase = {'a': 10, 'b': 34, 'A': 7, 'Z': 3}
{v: k for k, v in mcase.items()}

# %% [markdown]
# ## Set Comprehension

# %%
squared = {x**2 for x in [1, 1, 2]}
print(squared)

# %% [markdown]
# ## Generator Comprehension

# %%
multiples_gen = (i for i in range(30) if i % 3 == 0)
print(multiples_gen)
for x in multiples_gen:
  print(x)

# %% [markdown]
# ## Lambda Functions/Expressions
#
# lambda arguments: expression

# %%
double = lambda x: x * 2
print(double(5))


# %%
def myfunc(n):
  return lambda a: a * n

mydoubler = myfunc(2)

print(mydoubler(11))

# %% [markdown]
# Use lambda for custom sort

# %%
a = [(1, 2), (4, 1), (9, 10), (13, -3)]
a.sort(key=lambda x: x[1])
a

# %% [markdown]
# ## Map, Filter and Reduce

# %% [markdown]
# **map** applies function on list elements

# %%
seq = [1, 2, 3, 4, 5]
result = list(map(lambda var: var*2, seq))
print(result)

# %% [markdown]
# **filter** returns a subset of the original list

# %%
seq = [1, 2, 3, 4, 5]
result = list(filter(lambda x: x > 2, seq))
print(result)

# %% [markdown]
# **reduce** applies lambda/function on sequential pairs of values in a list

# %%
from functools import reduce
def do_sum(x1, x2): return x1 + x2
reduce(do_sum, [1, 2, 3, 4])

# %%
from functools import reduce
reduce((lambda x, y: x * y), [1, 2, 3, 4])

# %% [markdown]
# ## List slicing

# %%
mylist = ['one', 'two', 'three', 'four', 'five']
mylist[1:3]

# %%
mylist = ['one', 'two', 'three', 'four', 'five']
print(mylist[1:3])
print(mylist[1:])
print(mylist[:3])
print(mylist[:])

# %% [markdown]
# Lists and list slices are mutable

# %%
mylist = ['one', 'two', 'three', 'four', 'five']
mylist[1:3] = ['Hello', 'Guys']
print(mylist)

# %% [markdown]
# ## List Methods

# %% [markdown]
# - `list.insert(at, element)`
# - `list.append(element)`
# - `list.extend(list2)`
# - `list.sort()`
# - `list.reverse()`
# - `list.index(elem)`
# - `list.pop(idx)`
# - `list.pop()`
# - `list.remove(elem)`
# - `del list[idx]`
# - `len(list)`
# - `min(list)`
# - `max(list)`
# - `sum(list)`
# - `cmp(list1, list2)`
# - `list1 + list2` merge list
# - `list1 * 2` repeat list

# %% [markdown]
# ## Enumarate

# %%
alist = ['a1', 'a2', 'a3']

for i, a in enumerate(alist):
    print(i, a)

# %% [markdown]
# ## Zip
#
# Iterate of two lists.

# %%
alist = ['a1', 'a2', 'a3']
blist = ['b1', 'b2', 'b3']

for a, b in zip(alist, blist):
    print(a, b)

# %% [markdown]
# ## Enumarate Zip

# %%
alist = ['a1', 'a2', 'a3']
blist = ['b1', 'b2', 'b3']

for i, (a, b) in enumerate(zip(alist, blist)):
    print(i, a, b)

# %% [markdown]
# ## np.arrange and linspace

# %%
import numpy as np
# np.arange(start, stop, step)
np.arange(3, 7, 2)

# %%
# np.linspace(start, stop, num)
np.linspace(2.0, 3.0, num=5)

# %% [markdown]
# ## Pandas AXIS

# %% [markdown]
# ```
# df.drop('Row A', axis=0)
# df.drop('Column A', axis=1)
# ```
#
# `df.shape` returns a tuple of `(rows,columns)` with rows at `data[0]` and columns at `data[1]`.

# %% [markdown]
# ## Introspection

# %% [markdown]
# **dir()***

# %%
some_list = ['a', 'b', 'c', 'b', 'd', 'm', 'n', 'n']
dir(some_list)

# %% [markdown]
# **id()**

# %%
id(some_list)

# %% [markdown]
# **type**

# %%
type(some_list)

# %% [markdown]
# **inspect**

# %%
import inspect
print(inspect.getmembers(str))

# %% [markdown]
# ## Debugging

# %%
# # %debug → IPython Magic.

# %% [markdown]
# - `c`: continue execution
# - `w`: shows the context of the current line it is executing.
# - `a`: print the argument list of the current function
# - `s`: Execute the current line and stop at the first possible occasion.
# - `n`: Continue execution until the next line in the current function is reached or it returns.

# %%
import pdb

def make_bread():
    pdb.set_trace()
    return "I don't have time"

print(make_bread())
