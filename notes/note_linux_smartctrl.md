# Smartctrl

## Query state of drive

`smartctrl -a  -d megaraid,11  /dev/sda`

## Query temperature of drive

`smartctrl -A  -d megaraid,11  /dev/sda`

## Query SMART state of drive

`smartctrl -H  -d megaraid,11  /dev/sda`