# monit

> Monit {en} ist ein einfaches, aber effektives Programm zur Überwachung von Serverdiensten, kann aber auch wichtige Basisdaten wie CPU-Nutzung, Festplattenbelegung usw. einbeziehen. Falls ein Serverdienst ausfällt, kann er automatisch neu gestartet werden. Bei Problemen wird der Systemverwalter per E-Mail informiert. [^ubu]

## Installation

- `apt-get install -y monit`

## Konfiguration

Die Konfiguration von Monit erfolgt über die zentrale Konfigurationsdatei `monitrc`.
Hier kann beispielsweise die mit ausgelieferte Web-UI konfiguriert werden

- `vim /etc/monit/monitrc`

```
set httpd port 2812 and
    use address 192.168.23.21  # only accept connection from localhost
# allow 0.0.0.0/0.0.0.0       # allow localhost to connect to the server and
    allow admin:monit      # require user 'admin' with password 'monit'
```

## Quellen
[^ubu]: [Monit auf ubuntuusers.de](https://wiki.ubuntuusers.de/Monit/)