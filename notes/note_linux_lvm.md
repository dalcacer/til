# Logical Volume Management

As explained by [digitalocean](https://www.digitalocean.com/community/tutorials/an-introduction-to-lvm-concepts-terminology-and-operations).

LVM has

- physical volumes `pv..` physical block devices or other disk-like devices.
- volume groupes `vg...` combined `pv`s as storage pools.
- logical volumes `lv...` in `vg`

> In summary, LVM can be used to combine physical volumes into volume groups to unify the storage space available on a system. Afterwards, administrators can segment the volume group into arbitrary logical volumes, which act as flexible partitions.


## Volume and volume group management

- `lvmdiskscan` List available physical devices.
- `pvs` Display information about physical volumes.
- `vgs` Display information about volume groups.
- `lvs` Display information about logical volumes.
- `vgdisplay` Display volume group information.
- `lvdisplay` Display information about a logical volume.
- `pvcreate /dev/sda /dev/sdb` create lvm physical volume out of physcial device. (check with pvs).
- `vgcreate LVMVolGroup /dev/sda /dev/sdb` Create volume group. (check with pvs/vgs).
- `lvcreate -L 10G -n projects LVMVolGroup`
- `vgs -o +lv_size,lv_name`
- `lvcreate -l 100%FREE -n workspace LVMVolGroup`

## Format and mount

- `/dev/volume_group_name/logical_volume_name`
- `mkfs.ext4 /dev/LVMVolGroup/projects`
- `mkdir -p /mnt/projects`
- `vim /etc/fstab`
   - `/dev/LVMVolGroup/projects /mnt/projects ext4 defaults,nofail 0 0`

## Expand Volume Group

As explained by [thomas-krenn.com](https://www.thomas-krenn.com/de/wiki/LVM_vergr%C3%B6%C3%9Fern)

- `pvcreate /dev/sdc`
- `vgextend LVMVolGroup /dev/sdc`
- `lvextend -L 1G /dev/LVMVolGroup/projects`
- `resize2fs -p /dev/LVMVolGroup/projects`

## Mirroring

- `lvcreate -L 50M -m1 -n projects LVMVolGroup`

## Snapshotting

As spotted on [thomas-krenn.com](https://www.thomas-krenn.com/de/wiki/LVM_Snapshots)

- `lvcreate -l100%FREE -s -n SNAPSHOTVOLUME /dev/LVMVolGroup/projects` (`-s` does the trick.)