# systemctl

As explained by [readhat](https://www.redhat.com/sysadmin/getting-started-systemctl).


- `systemctl`
- `systemctl status sshd`
- `systemctl restart sshd`

> Reloading services is a little different. The reload subcommand only causes the service to re-read the configuration file, while the restart subcommand terminates all current connections and re-reads the configuration file.

- `systemctl reload sshd`
- `systemctl stop sshd`
- `systemctl start sshd`
- `systemctl kill sshd`
- `systemctl enable sshd`
- `systemctl disable sshd`
- `systemctl is-enabled sshd`
