# journalctl

A really neat CLI for linux system logging as explained by [howtogeek](https://www.howtogeek.com/499623/how-to-use-journalctl-to-read-linux-system-logs/) and [linuxhandbook](https://linuxhandbook.com/journalctl-command/).


- `journalctl`
- `journalctl -k` kernel only
- `journalctl -u ssh` ssh only
- `journalctl -p 3` error only
- `journalctl --no-page`
- `journalctl -n 20` 
- `journalctl -f`
- `journalctl -o short-full`
- `journalctl -o verbose`
- `journalctl -o json`
- `journalctl -o json-pretty`
- `journalctl --list-boots` 
- `journalctl -b -2` -2nd boot session only
- `journalctl -p 4..6 -b0`
- `journalctl -S "2020-12-31 07:00:00"` from (accepts various formats like today, yersterday, -1d, ...)
- `journalctl -S "2020-12-31 07:00:00" -U 2021-01-15` from to  
- `journalctl --disk-usage`

## Priorities

| Priority |  Code   |
|:--------:|:-------:|
|    0     |  emerg  |
|    1     |  alert  |
|    2     |  crit   |
|    3     |   err   |
|    4     | warning |
|    5     | notice  |
|    6     |  info   |
|    7     |  debug  |