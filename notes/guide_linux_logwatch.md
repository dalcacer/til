# Logwatch

> Logwatch {en} ist ein in Perl geschriebenes Tool zur Analyse von Logdateien. Es soll Systemadministratoren helfen, die Übersicht über alle Vorgänge auf einem Serversystem zu behalten. Logwatch durchsucht die Logdateien des Systems und generiert eine Kurzfassung daraus, deren Gestaltung individuell konfiguriert werden kann. Diese kann dann entweder als Datei weiterverarbeitet oder zum Versenden an einen Mailserver weitergereicht werden. [^ubu]


## Installation

- `apt-get install -y logwatch`
- `perl -MCPAN -e 'install Sys::CPU'`
- `perl -MCPAN -e 'install Sys::MemInfo'`


## Konfiguration
Die grundlegende Konfiguration von logwatch ist über die zentrale Konfigurationsdartei `/usr/share/logwatch/default.conf/logwatch.conf`
möglich. Eingestellt werden kann der Detailierungsgrad und das Format der Berichte, sowie, falls verwendet, ein E-Mail-Empfäger und Absender.

- `vim /usr/share/logwatch/default.conf/logwatch.conf`

```
Format = html
MailTo = someone@example.com
MailFrom = logwatch@HOSTNAME.example.com
Detail = High
```

### ZFS Statusreport hinzufügen

- http://zfswatcher.damicon.fi

- `vim /usr/share/logwatch/dist.conf/services/zz-zfs.conf`

```
[...]
$pathto_zpool = "/sbin/zpool"
$pathto_zfs = "/sbin/zfs"
[...]
```

## Ausführung

Einmal konfiguriert, kann logwatch durch den CLI-Befehl `logwatch` zur Ausführung gebracht werden.
Ohne weitere Angaben werden die Angaben der Konfigurationsdatei (`/usr/share/logwatch/default.conf/logwatch.conf`)
für die Erstellung des Reports herangezogen.
Die Befehlsvariation `logwatch --output mail --mailto recipient@example.com` schickt die Bericht unmittelbar an
die angegebene E-Mail-Adresse.

Es empfiehlt sich die regelmässige Ausführung von Logwatch, z.B. durch einen `/etc/cron.weekly/`-Eintrag.

- `vim /etc/cron.weekly/00logwatch`

```
#!/bin/bash

#Check if removed-but-not-purged
test -x /usr/share/logwatch/scripts/logwatch.pl || exit 0
#execute
/usr/sbin/logwatch --output mail
#Note: It's possible to force the recipient in above command
#Just pass --mailto address@a.com instead of --output mail
```

## Quellen
[^ubu]: [Logwatch auf ubuntuusers.de](https://wiki.ubuntuusers.de/Logwatch/)