# APT

> APT Advanced Package Tool "handles the installation and removal of software on Debian, Ubuntu, and related Linux distributions", [^wiki]

APT is a pretty nifty tool, but from time to time it runs into problems.

## IPV6

APT might have trouble connecting via IPV6.

- https://blog.friedlandreas.net/2016/07/ubuntu-16-04-apt-get-hat-probleme-mit-ipv6/

```sh
apt-get -o Acquire::ForceIPv4=true update
apt-get -o Acquire::ForceIPv4=true upgrade
```

you can also run

`echo 'Acquire::ForceIPv4 "true";' | tee /etc/apt/apt.conf.d/99force-ipv4`


🚨 Not needed with 18.04?????

## add-apt command

`add-apt` is awesome for managing package sources. Some Ubuntu server installation miss this command. 

Under 16.04 it can be installed via `apt-get install software-properties-common`

## Automatic updates

Server software needs recurring maintenance and updates. By installing `unattended-upgrade` you can update critical security patches with ease.

- `apt-get install unattended-upgrades`

- **Selection of update sources**
- `dpkg-reconfigure -plow unattended-upgrades`

- **Activate automatic updates**
- `vim /etc/apt/apt.conf.d/10periodic`

```sh
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::Unattended-Upgrade "1";
```

[^wiki]: [https://en.wikipedia.org/wiki/APT_(software)]