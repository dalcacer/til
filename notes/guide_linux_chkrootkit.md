# Setup and administer chkrootkit

> Chkrootkit {en} bietet eine Möglichkeit, Linuxsysteme auf Anzeichen eines Einbruchs zu untersuchen. [^ubu]

## Installation

- `apt-get -y install chkrootkit`

## Automatic excetution

* `crontab -e`
```
0 3     * * *   root    (cd /usr/sbin; ./chkrootkit 2>&1 | mail -s "chkrootkit output" xxx@xxx.xxx)
```

## Sources

[^ubu]: [chkrootkit auf ubuntusers.de](https://wiki.ubuntuusers.de/chkrootkit)