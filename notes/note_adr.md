# Architecture Decision Records

As explained by 
- [CodeOpinion](https://www.youtube.com/watch?v=6H6zfCNeqek)
- [Nygard Documenting Architecture Decisions](https://cognitect.com/blog/2011/11/15/documenting-architecture-decisions)


Document an architctural decision with
- `title`
- `context`
- `decision`
- `status`
- `consequences`

- https://github.com/joelparkerhenderson/architecture-decision-record

- https://github.com/npryce/adr-tools