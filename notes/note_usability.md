# Usability und UX

Zusammenfassung nach Michael Richter, Markus Flückiger: Usability und UX kompakt

## Usability und UX

Different research areas/thematic fields

- Human Computer Interaction
- Human Factors
- Interaction Design
- Usability Engineering
- User Centered Design
- User Experience
- Design Thinking


Usabilty can only be seen and understood in the context of a user performing a task with a tool.

Gebrauchstauglichkeit: das Ausmaß, in dem ein Produkt durch bestimmte Benutzer in einem bestimmten Nutzungskontext genutzt werden kann, um bestimmte Ziele effektiv, effizent und zufriedenstellend zu erreichen ISO 9241-11

Include aestethics and the "joy of use"

User Experience: Wahrnehmung und Reaktion einer Person, die aus der tatsächlichen und/oder erwarteten Benutzung eines Produkts, eines Systems oder Dienstleistung resultiert ISO 9241-210

## Nutzungsorientierung im Entwicklungsprozess

ISO 9241-210 zu *Gestaltung gebrauchstauglicher interaktiver Systeme*

- die Gestaltung beruht auf einem umfassenden Verständnis der Benutzer, Arbeitsaufgaben und Arbeitsumgebungen.
- die Benutzer sind während der Gestaltung und Entwicklung einzubeziehen
- das Verfeinern und Anpassen von Gestaltungslösungen wird fortlaufend auf der basis benutzerzentrierter Evaluierungen vorangetrieben
- der Prozess ist iterativ
- bei der Gestaltung wir die gesamte User Experience berückstichtigt
- im Gestaltungsteam sind fachübergreifende Kentnisse und Perspektiven vertreten

- Contextual Inquiry: "In Beobachtungen und Befragungen der Benutzer vor Ort werden die konkretren Aufgaben, Abläufe und Verhaltensmuster sowie die Umgebung der Anwendung analyisiert, ausgewerte und dokumentiert"
Personas und Szenarien auf Basis der Inquiry.

- Personas: Prototypische Benutzerprofile
- Szenarien: Anwendung des Produkts aus Benutzersicht.

Use Cases und Stories auf Basis von Personas und Szenarien.

Use User Story Map, Storyboards, UX Prototypes, Guidelines and Styleguides.

Accompaning usability tests for development or as deliverable. Usability walkthroughs lesser formal ideal for develping first prototypes.

Questionnaires

## Die 7+2 wichtigsten Methoden

- Contxtual Inquiry: Analyse der Benutzer und des Einsatzumfeldes des neuen Systems
- Personas und Szenarien: Modelliergen der unterschiedlichen Benutzergruppen und der Anwendung aus Benutzersicht
- Storyboards: Kommunizieren ausgewählter Abläufe mit dem neuen System
- UX Prototyping: Entwickeln von Produktideen, Klären der Anforderungen, Konzipieren und Optimieren der Benutzerschnittstelle
- User Cases und User Stories: Funktionale Anforderungen in die Entwicklung tragen
- Guidelines and Styleguides: Definieren der Gestaltungsrichtlinien
- Usability Testing: Beurteilen des neuen Systems durch Benutzer
- Fragebögen: Sammeln aussagekräftiger Zahlen zu Analyse von Benutzern und Kontext oder zur Beurteilung eines Systems oder Prototyps.

## Contextual Inquiry

Untersuchung der Bedürfnisse der Benutzer in ihrem Umfeld. Beobachtung und Intviews.

Fragestellungen die via Contextual Inqury erarbeitet werden können Tab 4.2 

"Die Methode zeigt verbreitete Muster, erprobte Lösungsansätze, Bedürfnisse und ungelöste Probleme der Menschen auf - Faktoren, die Potenzial für wirklich nützliche Produkte bieten, welche die Benutzer ansprechen."

Verfahren wird mit Aufwandsschätzung im Buch zusammengefasst (S. 56).

## Personas und Szenarien

**Personas**

Motivation: n Nutzer aus der selben Benutzergruppe haben unterschiedilche Vorstellung.

Prototypische Benutzer mit unterschiedlichen Zielen, Verhaltensweisen, Eigenschaften in Hinblick auf zu entwickelndes Produkt.

- Ziele.
- Beruf, Funktion, Verantwortlichkeiten, Aufgaben.
- Fachliche Ausbildung, Wissen und Fähigkeiten.
- Verhaltensmuster und Vorgehensweisen.
- Werte, Ängste, Sehnsüchte, Vorlieben.
- Allgemeine Computerkenntnisse.
- Kenntnisse über verwandte Produkte, Vorgängersysteme, Konkurenzprodukte.
- Verbesserungspotential der heutigen Sitation,
- Erwartungen an die neue Lösung.

Personas eingängig gestalten

- Name, Alter, Geschlecht.
- Markige Charakterzüge.
- Bild, Skizze, Portrait.
- Zitate aus Interviews.
- Ein Tag im Leben von...

Unterteilung in 

- Primäre Personas
- Sekundäre Personas: kleine Erweiterungen gegenüber primäre Personas.
- Ergänzende Personas: kaum Erweiterung gegenüber primaäre Personas.
- Non-Persona: explizit nicht berücksichtigt

Good, vivid example at [Gitlab.com](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/).

**Szenarien** zur 

- Erhebung und Validierung von Anforderungen
- Spezifikation
  - Ergänzung von Use Cases
  - Quelle von User Stories
- User Interface Konzept
- Usability Testszenarien
- Testszenarien
- Schulungen

Szenarien könnnen geggenüber formalen Spezifikationen (Stichwort Anforderungen) Vorteile aufweisen, weil sie in epischer Form mehr details aufdecken.

Verfahren wird mit Aufwandsschätzung im Buch zusammengefasst (S. 66).

## Storyboards

- Visualiserung von Szenarien.
- User Interface only oder gesamter Benutzungskontext
- Visualiert gut spezielle oder besondere Umgebungen

Verfahren wird mit Aufwandsschätzung im Buch zusammengefasst (S. 72).

## UX Prototyping

Prototypen mit unterschiedlichem

- Funktionsumfang: Welche Funktionen
- Funktionstiefe: Wie detailiert
- Darstellungstreue: Handskizze ➡️ Drahtmodell ➡️ UI
- Interaktivität: Statisch, z.B. Papier/Pappe? Dynamisch?
- Datengehalt:
- Technische Reife:

"**Jeder Prototyp stellt einen Kompromiss zwischen notwendigem Aufwand und Zweck dar**"

**User Interface Konzept** erarbeiten.
- Menüs und Dialoge.
- Informationen und Darstellung.
- Technologien, e.g. touch devices.

Inhalte eine User Interface Konzepts

- Zielplattform
- Grundsätzlicher Aufbau und Screen Design
- Anzeige- und Eingabegeräte
- Aufteilung und Strukture von Informationen,
- Verwendung und Verhalten von Fenstern,
- Wichtige Bedienelemente,
- Navigation mittels Menüsm, Schaltflächen und Links,
- Prüfung von Eingaben und Anzeigen von Fehlermeldungen,
- Konzepte für das Speichern von Informationen und Zuständen,
- Rückgängig machen, erneut ausführen,
- Interaktionsprinzipien wie Gesten, direkte Maniuplation, Dragn & Drop oder Kontextmenüs.
 
 Unterschiedliche Ziele können mit unterschiedliche detailierten Prototypen unterstützt werden. Tab. 4.4. (S. 79) benennt die folgenden Ziele
 
 - Produktidee entwickeln: Kernfunktion. Kaum Funktionstiefe. Keine Darstellungstreue.
 - Anforderungen schärfen: Funktionsumfang mit realistischen Daten.
 - Benutzerschnittstelle konzipieren: Mittlere Darstellungstreue. Ausgewählte Funktionen im Detail. Teilweise interaktiv.
 - Benutzerschnittstelle optimieren: Hohe Darstellungstreue. Interaktiv für ausgewählte Funktionen.
 - Für gutes Aussehen sorgen: Hohe Darstellungstreue.
 - User Interface spezifizieren: Funktionsumfang und -tiefe mittel bis hoch. Mittlere Interaktivität.
 
**Paper Prototype**

- Jeder kann beitragen
- Schnell erstellt
- Kaum Zeit auf Details verwendet
- Technsich einfach zu realisieren
- Neue UI Elemente schnell entworfen
- Interaktiv/Kooperativ in der Gruppe
- Revidieren und Neuanfangen einfach

**UX Prototyping**

- iterativer Prozesse
- schnelle Iterationen mit Feedback besser als Perfektionismus

Verfahren wird mit Aufwandsschätzung im Buch zusammengefasst (S. 84).

## Use Cases and User Stories

"User Cases werden eingesetzt, um das Verhalten des künftigen Systems zu vereinbaren und festzuhalten", "User Stories dagegen halten lediglich eine Kommunikationsabsicht fest."

**Use Cases**

Well known. Ain't nobody got time for that/ain't repeating that stuff.

**User Stories**

- [Atlassian about User Stories](https://www.atlassian.com/agile/project-management/user-stories)
- [Nikita Sobolev's - Engineering Guid to User Stories](https://sobolevn.me/2019/02/engineering-guide-to-user-stories)

**User Story Map**

Backlog zum priorisieren der User Stories.
Zeitlich und entsprechend des grobane Ablaufs sortieren.


Verfahren wird mit Aufwandsschätzung im Buch zusammengefasst (S. 95).

## Gudielines and Styleguides
## Usability Testing
## Questionnaires


# References

