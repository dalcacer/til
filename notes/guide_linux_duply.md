# Duply

As explained by [Thomas Krenn](https://www.thomas-krenn.com/de/wiki/Backup_unter_Linux_mit_duply) and  [Dirk Ruediger](https://niebegeg.net/post/32010111657/duplicity-backups-wiederherstellen/).

> Duply is a simple python script that allows to create incemental, symertically encrypted backups on file basis. It servers as fronted for [duplicity](http://duplicity.nongnu.org/). Backups can be locally or remote. Duply supports ftp, ssh, s3, rsync, cifs, webdav, http abd others.

A backup can be achieved by creating profiles. Profiles can contain `pre` and `post` scripts. Profiles can be adjusted with a `conf` file and by maintaining an `exclude` list.

## Installation

- `apt install -y duplicity duply`

## Creating a backup profile

- `duply profilename create`
- `vim ~/duply/profilename/conf`
- Adjust `SOURCE=`
- Adjust `TARGET=`

## Configuring the strategy and rotation

- `vim ~/duply/profilename/conf`
- `MAX_AGE=1M`
- `MAX_FULL_BACKUPS=1`

## Perform a backup

- Full `duply profilename full`
- Incremental `duply profilename incr`

## Using cron jobs 

```sh
0 0 * * 7 /usr/bin/duply /root/.duply/profilename full_verify_purge --force
0 0 * * 1-6 /usr/bin/duply /root/.duply/profilename incr
```

> In this configuration a full backup will be made sundays at 00:00. All deprecated backups will be deleted. An incremental backup will be created each day from monday to saturday.

## Checking the backup state

- `duply profilename status`

## Restoring from a backup

- `duply profilename restore targetpath` Will perform a full restoration to that path.
