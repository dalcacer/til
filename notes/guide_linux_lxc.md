# LXC

> LinuX Container (LXC) bieten die Möglichkeit, Prozesse und Prozessgruppen zu isolieren, indem Kernel-Ressourcen virtualisiert und gegeneinander abgeschottet werden. Dies wird mit Hilfe der im Kernel enthaltenen Cgroups realisiert. Mit LXC kann man entweder einzelne Anwendungen oder ganze Linux-Distributionen in einem Container starten. Ähnliche Konzepte gibt es bei Linux-VServer oder Jails (FreeBSD) oder Zones (Solaris).[^ubu]
> The goal is to offer a distro and vendor neutral environment for the development of Linux container technologies.[^con]

## Installation

- `apt install -y lxc`

## Anlegen eines neuen Containers

LXC-Container können aus verschiedenen `Templates` erstellt werden. 
Das Anlegen eines neuen Containers in einer interaktiven Sitzung (die Wahl welcher Distribution und Architektur zu verwenden ist) kann wie folgt erfolgen:

- `lxc-create -n containername -t download`

## Starten eines Containers

- `lxc-start -n containername`
- `lxc-start -n containername -d`

## Auflisten aller laufenden Container

- `lxc-ls`
- `lxc-ls -f`  -- umfassendere Ausgabe

**Beispiel**

```
root@host:/# lxc-ls -f
NAME      STATE   AUTOSTART GROUPS IPV4           IPV6                               
docs      RUNNING 1         -      192.168.178.41 2003:a:b5f:b200:216:3eff:fec5:2f36 
gitlab    RUNNING 1         -      192.168.178.38 2003:a:b5f:b200:216:3eff:fed8:8cf6 
gl_mirror RUNNING 1         -      10.0.3.132     -                                  
gl_r1     RUNNING 1         -      192.168.178.40 2003:a:b5f:b200:216:3eff:feb9:34b0 
```

## Laufzeitinformationen zu einem Container

 - `lxc-info -n containername`
 
 **Beispiel**
```
root@host:/# lxc-info -n gitlab
Name:           gitlab
State:          RUNNING
PID:            4351
IP:             192.168.178.38
IP:             2003:a:b5f:b200:216:3eff:fed8:8cf6
CPU use:        44510.12 seconds
BlkIO use:      25.11 GiB
Memory use:     3.96 GiB
KMem use:       0 bytes
Link:           veth9BKFLC
TX bytes:      321.31 MiB
RX bytes:      1.21 GiB
Total bytes:   1.52 GiB
```

## Terminalzugang zu einen Container

- `lxc-attach -n containername`

## Einfrieren eines Containers

- `lxc-freeze -n containername`

## Auftauen eines Containers

- `lxc-unfreeze -n containername`

## Stoppen eines Containers

- `lxc-stop -n containername`

## Zerstören eines Containers

- `lxc-destroy -n containername`

## Duplizieren eines Containers

- `lxc-copy -n gitlab -N gitlabNEW`

## LXC Konfigurieren

**IPv6 Passthrough**

    lxc.network.type = veth
    lxc.network.link = br0
    lxc.network.flags = up
    lxc.network.hwaddr = 00:16:3e:c5:2f:36

**NAT**

    lxc.network.type = veth
    lxc.network.link = lxcbr0
    lxc.network.flags = up
    lxc.network.hwaddr = 00:16:3e:58:c5:09


## Container Konfigurieren

Sämtliche Container werden unter den Pfad `/var/lib/lxc/` angelegt.

In einem containerspezifischem Ordner werden jeweils das `rootfs` und eine Konfigurationsdatei `config` abgelegt.
Über die conf-Datei kann die Hardwarebelegung (RAM/CPU), die Netzwerkkonfiguration oder das Startverhalten (`lxc.start.auto` für das automatische Starten bei Boot des Hostsystems) konfiguriert werden.

    # Template used to create this container: /usr/share/lxc/templates/lxc-download
    # Parameters passed to the template:
    # For additional config options, please look at lxc.container.conf(5)

    # Uncomment the following line to support nesting containers:
    #lxc.include = /usr/share/lxc/config/nesting.conf
    # (Be aware this has security implications)

    # Distribution configuration
    lxc.include = /usr/share/lxc/config/ubuntu.common.conf
    lxc.arch = x86_64

    # Container specific configuration
    lxc.rootfs = /var/lib/lxc/docs/rootfs
    lxc.rootfs.backend = dir
    lxc.utsname = docs

    # Network configuration
    lxc.network.type = veth
    lxc.network.link = br0
    lxc.network.flags = up
    lxc.network.hwaddr = 00:16:3e:c5:2f:36
    lxc.start.auto = 1

Durch die transparente Einbettung des Dateisystems der Container in dem Pfad `/var/lib/lxc/containername/rootfs/` kann das Hostsystem lesend und schreibend auf den Gast zu greifen.
Dies kann für gemeinsame Verwendung systemweiter Ressourcen (wie z.B. durch Wirt und Gast geteilte HTTPS-Zertifikate) ausgenutzt werden.

Zusätzelich kann die LXC-Konfiguration dazu genutzt werden, um Wirt-Ordner mit mehreren Gast-Ordnern zu teilen.

**Wirt**

* `mkdir /media/data/share && chmod 7777 /media/data/share`

**Gast**

* `mkdir /share`

**Gast-Konfiguration** 

* `/var/lib/lxc/containername/config`
* `lxc.mount.entry = /media/data/share share none ro,bind 0.0`

[^aksubushare]

### cgroups

[thomas-krenn.com](https://www./de/wiki/Cgroup_Werte_von_LXC_Linux_Containern)

| subsystem  | Zweck                                                                                       |
| :---:      | :----:                                                                                      |
| cpusets    | Limits für CPUs und Memory Nodes für Tasks einer Gruppe.                                    |
| blkio      | Limits für Input/Output Zugriffe auf Block Devices.                                         |
| cpuacct    | Limits für CPU Ressourcen von Tasks einer Gruppe.                                           |
| devices    | Erlaubt oder verbietet Zugriffe auf Devices für Gruppen.                                    |
| freezer    | Suspend oder Resume von Tasks.                                                              |
| hugetlb    | Limits für Virtual Memory Tables.                                                           |
| memory     | Limits für Memory für Tasks in einer Gruppe.                                                |
| net_cls    | Tagged Netzwerk-Pakete mit Class IDs, ein Traffic Controller kann IDs Prioritäten zuordnen. |
| net_prio   | Prioritäten für Netzwerk-Traffic von Tasks einer Gruppe.                                    |
| cpu        | CPU Scheduling Verwaltung, zusammen mit cpuacct.                                            |
| perf_event | Monitoring Threads einer Task Gruppe auf einer CPU.                                         |


## Container Sicherung während des Betriebs

Durch die transparente Einbettung des Dateisystems der Container in dem Pfad `/var/lib/lxc/containername/rootfs/` kann das Hostsystem lesend und schreibend auf den Gast zu greifen.
Eine dateisystembasierte Sicherung, die folgende durch den Betrieb des Containers in Verwendung befindlichen Pfade ignoriert ist denkbar.

```
- /var/lib/lxc/*/rootfs/lost+found
- /var/lib/lxc/*/rootfs/media/*
- /var/lib/lxc/*/rootfs/mnt/*
- /var/lib/lxc/*/rootfs/proc/*
- /var/lib/lxc/*/rootfs/run/*
- /var/lib/lxc/*/rootfs/sys/*
- /var/lib/lxc/*/rootfs/tmp/*
```

Further you can provide a custom backup script

- `vim /usr/bin/lxcbackup`


```sh
#!/bin/bash
START=$(date +%s.%N)
TODAY=$(date --iso-8601)
BACKUP=backup-`date +%s`.tar.gz

LXC_DIR=/var/lib/lxc
REMOTE_HOST=YOURHOST
REMOTE_DIR=/PATH/TO/MONHTLY/BACKUP

for container_path in "$LXC_DIR"/*
do
  container_name="$(basename $container_path)"
  echo "clearing apt caches for $container_name"
  lxc-attach --name ${container_name} apt clean
  echo "stopping $container_name"
  lxc-stop -n ${container_name}
  cd $container_path
  echo "compressing $container_name"
  echo "${LXC_DIR}/${TODAY}-${container_name}.tar"
  tar --numeric-owner -czvf ${LXC_DIR}/${TODAY}-${container_name}.tar ./*
  echo "starting $container_name"
  lxc-start -n ${container_name} --daemon
done

echo "moving existing remote files to temporary remote folder"
ssh $REMOTE_HOST mkdir ${REMOTE_DIR}/old-${TODAY}
ssh $REMOTE_HOST mv ${REMOTE_DIR}/*.tar ${REMOTE_DIR}/old-${TODAY}

echo "copying backups to ${REMOTE_HOST}:${REMOTE_DIR}"
rsync -avzhe ssh --progress ${LXC_DIR}/*.tar ${REMOTE_HOST}:${REMOTE_DIR}

echo "removing temporary remote folder"
ssh $REMOTE_HOST rm -rf ${REMOTE_DIR}/old-${TODAY}

echo "removing local tars"
rm ${LXC_DIR}/*.tar
```

- `crontab -e`

- Perform a full backup every first sunday in a month at 01:30

```sh
00 2 1-7 * 7 /usr/bin/lxcbackup
```



## Quellen

[^ubu]: [LXC auf ubuntuuser.de](https://wiki.ubuntuusers.de/LXC/)
[^con]: [Linux Containers](https://linuxcontainers.org)
[^aksubushare]: [Share Between LXC Container and Host on Aksubuntu.com](http://askubuntu.com/questions/610513/how-do-i-share-a-directory-between-an-lxc-container-and-the-host)