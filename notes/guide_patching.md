# Patch software projects

Under some circumstances it is necessary to modify software projects, e.g. to enable cross compilation.

It is possible to store these alterations in an reproducible manner by creating **patch files**.
By doing this changes can be sent and applied by others.


## Create patches

1. Download software project
2. Craft two copies, e.g.
    - `sw-project-1.0.0`
    - `sw-project`
3. Perform alterations in `sw-project`
4. Create patch by running `command diff -Nru sw-project-1.0.0 sw-project > sw-project.patch`

The file `sw-project.patch` contains all the differences between the original project and the alteration.

## Apply patches

1. Download software project
2. Apply patch `patch -p1 -i sw-project.patch`

