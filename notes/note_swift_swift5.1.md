# Swift 5.1

As explained by Paul Hudson [on youtube](https://www.youtube.com/watch?v=WQ-wppW8jt4) and [hackingwithswift.com](https://www.hackingwithswift.com/articles/182/whats-new-in-swift-5-1)

## Memberwise Initializers

```swift
struct User {
    var name: String
    var loginCount: Int= 0
}

let gloria = User(name: "Gloria Mandoza", loginCount: 0)
let suzanne = User(name: "Suzanne Warren")
```


## Implicit Returns

Omitting `return` works in simple functions.

```swift
let doubled1 = [1,2,3].map{ $0 * 2 }
let doubled2 = [1,2,3].map{ return $0 * 2 }

func double(_ value: Int) -> Int {
    value * 2
}
```

## Universal Self

```swift
class NetworkManager{
    class var maxActiveRequests:Int{
        return 4
    }

    func printDebug(){
        print("Maximum active requests \(NetworkManager.maxActiveRequests)")
    }
}

class ThrottledNetworkManager: NetworkManager{
    override class var maxActiveRequests:Int{
        return 1
    }
}

var manager = ThrottledNetworkManager()
manager.printDebug() //will print 4
```

We need to change `NetworkManager.maxActiveRequests` to `Self.maxActiveRequests`.

## Opaque return types

**Standard:** Let the user decide which return type.

```swift
protocol ImperialFighter{
    init()
}

struct TIEFighter: ImperialFighter {}
struct TIEAdvanved: ImperialFighter {}

func launchImperialFighter<T: ImperialFighter>() -> T {
    T()
}

let fighter1: TIEFighter = launchImperialFigher()
let fighter2: TIEAdvanced = launchImperialFigher()
```

**Now:** Let the function decide which return type.

```swift
protocol Fighter { }
struct XWing:Fighter { } 

func launchFighter() -> some Fighter{
    XWing()
}

let red5 = launchFighter()
```

## Static and class subscripts


```swift
public enum OldSettings{
    private static var values = [String: String]()

    static func get(_ name: String) -> String? {
        return values[name]
    }

    static func set(_ name; String, to_ newValue: String?) {
        print("Adjusting \(name) to \(newValue ?? "nil")")
        values[name] = newValue
    }
}

OldSettings.set("Captain", to: "Gary")
OldSettings.set("Friend", to: "Mooncake")
print(OldSettings.get("Captain") ?? "Unknown")
```

can be rewritten to 

```swift
public enum NewSettings {

    private static var values = [String: String]()
    public static subscript(_ name: String() -> String? {
        get {
            return values[name]
        }
        set{
            print("Adjusting \(name) to \(newValue ?? "nil")")
            values[name] = newValue
        }
    }
}

NewSettings["Captain"] = "Gary"
NewSettings["Friend"] = "Mooncake"
print(NewSettings["Captain"] ?? "Unknown")
```

### Ambigious none ecases

### Matching optional enums against non-optionals

```swift
enum BuildStatus{
    case starting
    case inProgress
    case complete
}

let status: BuildStatus? = .inProgress

switch status{
    case .inProgress:
        print("Build ist starting...")
    case .complete
        print("Build is complete.")
    default:
        print("Some other build status.")
}
```

### Ordered colection diffing

### Creating uninitialized arrays

```swift
let randomNumbers = Array<Int>(unsafeInitializedCapacitz: 10){ buffer, initializedCount in
    for x in 0..10 {
        buffer[x] = Int.random(in: 0...10)
    }
    initializedCount = 10
}


let randomNumbers2 = (0...9).map { _ in Int.random(in: 0...10) }
```