
# Swift Datatypes

## Protocols and Extensions

```swift
protocol P {}
class C: P {}

extension C { func foo() { print("C" )  } }
extension P { func foo() { print("P" )  } }

let c: C = C()
c.foo()      // will print C

let p: P = c
p.foo()      // will print P
```


```swift
protocol P { func foo() }
class C: P {}

extension C { func foo() { print("C" )  } }
extension P { func foo() { print("P" )  } }

let c: C = C()
c.foo()      // will print C

let p: P = c
p.foo()      // will print C
```

## Typ-Aliase

```
typealias A = Int

var a: A
var int: Int

a = int
int = a
```

## Tupel

```
let t = (foo: 0, bar: 1)
```

## Optionals

## Void

## Closures

## Generics

```swift
class Collection<Element> {
    func foo<T>(t: T) -> Element {...}
}

let c = Collection<String>
c.foo(t: Int)

let c2: Collection<String> = Collection
```

### Associated Types

```swift
protocol Collection {
    associatedtype Element
    func foo<T>(t: T) -> Element
}

class MyCollection{
    typealias Element = Int
    func foo<T>(t: T) -> Element
}

class MyCollection2{
    struct Element {}
    func foo<T>(t: T) -> Element
}
```

### Self

```swift
protocol Colection {
    func append(other: Self) -> Self
}

final class MyCollection: Collection {
    func append(other: MyCollection) -> MyCollection {...}
}
```