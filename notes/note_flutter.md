# Flutter

As explained by

- [Flutter get started](https://docs.flutter.dev/get-started/test-drive?tab=terminal#create-app)
- [Flutter for iOS developers](https://docs.flutter.dev/get-started/flutter-for/ios-devs)
- [Flutter Crash Course for Beginners 2021 - Build a Flutter App with Google's Flutter & Dart](https://www.youtube.com/watch?v=x0uinJvhNxI)
- [Building for iOS with Flutter (Google I/O'19)](https://www.youtube.com/watch?v=ZBJa-xjZl3w)
- [Flutter’s Cupertino Package for iOS devs - Flutter In Focus](https://www.youtube.com/watch?v=3PdUaidHc-E)
- [5 19 Introducing Cupertino Widgets](https://www.youtube.com/watch?v=kDXaYMFOxsY)


## Flutter on macOS Monterey M1 2020 - as of 2022-01

- `brew install openjdk@11` (higher 11 will cause `gradle` to fail)
- `export JAVA_HOME="/usr/local/homebrew/opt/openjdk@8"`
- install [Android Studio preview](https://developer.android.com/studio/preview)
  - ⚠️ The Android emulator will behave slowish and buggy. You should disable `Preferences/Tools/Emulator` → `Launch in tool window`
- install [Flutter](https://docs.flutter.dev/get-started/install)

##  Setting up a project

- `flutter create app_name`
- `cd flutter_testapp`
- `flutter devices`
- `flutter run`

You can enable web and desktop Apps

- **Web**
- `flutter config --enable-web`
- `flutter run -d chrome`

- **MacOS**
- `flutter config --enable-macos-desktop`
- `flutter run -d macos`

You can customize the setup
- `flutter create --project-name myapp --org dev.flutter --android-language java --ios-language objc myapp`
  - Valid languages `android:java/kotlin` and `ios:swift/objc`.

## Basics

### Flutter for iOS devs

#### Stateless/Stateful Widgets

> `StatelessWidgets` are useful when the part of the user interface you are describing does not depend on anything other than the initial configuration information in the widget.
> For example, in iOS, this is similar to placing a UIImageView with your logo as the image. If the logo is not changing during runtime, use a StatelessWidget in Flutter.
> If you want to dynamically change the UI based on data received after making an HTTP call, use a `StatefulWidget`. 
> [...]
> ❗️ The important difference between stateless and stateful widgets is that StatefulWidgets have a State object that stores state data and carries it over across tree rebuilds, so it’s not lost. -- [Flutter for iOS developers](https://docs.flutter.dev/get-started/flutter-for/ios-devs)

Example on using a stateless `Text`-Widget in a stateful manner.

```Dart
class SampleApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sample App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SampleAppPage(),
    );
  }
}

class SampleAppPage extends StatefulWidget {
  SampleAppPage({Key key}) : super(key: key);

  @override
  _SampleAppPageState createState() => _SampleAppPageState();
}

class _SampleAppPageState extends State<SampleAppPage> {
  // Default placeholder text
  String textToShow = "I Like Flutter";
  
  void _updateText() {
    setState(() {
      // update the text
      textToShow = "Flutter is Awesome!";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Sample App"),
      ),
      body: Center(child: Text(textToShow)),
      floatingActionButton: FloatingActionButton(
        onPressed: _updateText,
        tooltip: 'Update Text',
        child: Icon(Icons.update),
      ),
    );
  }
}
```

#### Layouting

- `padding: EdgeInsets.*`

```Dart
@override
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(
      title: Text("Sample App"),
    ),
    body: Center(
      child: CupertinoButton(
        onPressed: () {
          setState(() { _pressedCount += 1; });
        },
        child: Text('Hello'),
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
      ),
    ),
  );
}
```

[Layout widgets](https://docs.flutter.dev/development/ui/widgets/layout) lists

- Single-child layout widgets
  - `Align` - A widget that aligns its child within itself and optionally sizes itself based on the child's size.
  - `AspectRatio` - A widget that attempts to size the child to a specific aspect ratio.
  - `Baseline` - A widget that positions its child according to the child's baseline.
  - `Center` - A widget that centers its child within itself.
  - `ConstrainedBox` - A widget that imposes additional constraints on its child.
  - `Container` - A convenience widget that combines common painting, positioning, and sizing widgets.
  - `CustomSingleChildLayout` - A widget that defers the layout of its single child to a delegate.
  - `Expanded` - A widget that expands a child of a Row, Column, or Flex.
  - `FittedBox` - Scales and positions its child within itself according to fit.
  - `FractionalSizedBox` - A widget that sizes its child to a fraction of the total available space. For more details about the layout algorithm, see RenderFractionallySizedOverflowBox.
  - `IntrinsicHeight` - A widget that sizes its child to the child's intrinsic height.
  - `IntrinsicWidth` - A widget that sizes its child to the child's intrinsic width.
  - `LimitedBox` - A box that limits its size only when it's unconstrained.
  - `Offstage` - A widget that lays the child out as if it was in the tree, but without painting anything, without making the child available for hit...
  - `OverflowBox` - A widget that imposes different constraints on its child than it gets from its parent, possibly allowing the child to overflow the parent.
  - `Padding` - A widget that insets its child by the given padding.
  - `SizedBox` - A box with a specified size. If given a child, this widget forces its child to have a specific width and/or height (assuming values are...
  - `SizedOverflowBox` - A widget that is a specific size but passes its original constraints through to its child, which will probably overflow.
  - `Transform` - A widget that applies a transformation before painting its child.
- Multi-child layout widgets
  - `Column` - Layout a list of child widgets in the vertical direction.
  - `CustomMultiChildLayout` - A widget that uses a delegate to size and position multiple children.
  - `Flow` - A widget that implements the flow layout algorithm.
  - `GridView` - A grid list consists of a repeated pattern of cells arrayed in a vertical and horizontal layout. The GridView widget implements this component.
  - `IndexedStack` - A Stack that shows a single child from a list of children.
  - `LayoutBuilder` - Builds a widget tree that can depend on the parent widget's size.
  - `ListBody` - A widget that arranges its children sequentially along a given axis, forcing them to the dimension of the parent in the other axis.
  - `ListView` - A scrollable, linear list of widgets. ListView is the most commonly used scrolling widget. It displays its children one after another in the scroll direction....
  - `Row` - Layout a list of child widgets in the horizontal direction.
  - `Stack` - This class is useful if you want to overlap several children in a simple way, for example having some text and an image, overlaid with...
  - `Table` - A widget that uses the table layout algorithm for its children.
  - `Wrap` - A widget that displays its children in multiple horizontal or vertical runs.
- Sliver widgets
  - CupertinoSliverNavigationBar
  - CustomScrollView
  - SliverAppBar
  - SliverChildBuilderDelegate
  - SliverChildListDelegate
  - SliverFixedExtentList
  - SliverGrid
  - SliverList
  - SliverPadding
  - SliverPersistentHeader
  - SliverToBoxAdapter

> Use a Container when you want to add padding, margins, borders, or background color, to name some of its capabilities. --- [Layouts in Flutter](https://docs.flutter.dev/development/ui/layout)

