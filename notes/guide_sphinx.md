# Sphinx

> "Sphinx is a [command line] tool that makes it easy to create intelligent and beautiful [multi format] documentation [...]"
-- [sphinx-doc.org](http://sphinx-doc.org)

**Used all around the web**

- [readthedocs.org](https://readthedocs.org) → Public hosting
- [lab.uberspace.de](https://lab.uberspace.de) → Beautifully customized
- [docs.mattermost.com](http://docs.mattermost.com) → Beautifully customized
- ...
- **Inspired** [gitbook.com](https://gitbook.com)
- takes directory of **restructured text** files (ReSt)
- creates documentation as HTML/PDF/ePub/...
- extension available → [sphinx-doc.org](http://www.sphinx-doc.org/en/master/usage/extensions/index.html)
- themes available → [sphinx-doc.org](http://www.sphinx-doc.org/en/master/theming.html)
- configuration via Python 🐍 file
- customization via Python 🐍

## Hands on

#### Creating a project

- `pip install sphinx` **or** `conda install sphinx`
- `mkdir sphinx-test && cd sphinx-test`
- `sphinx-quickstart`
 - `> Separate source and build directories (y/n) [n]: y`
 - `> Project name: Test Project`
 - `> Author name(s): The Author`
 - `> Project release []: 1.0.0`
 - `> Project language [en]: `

**`tree -L 2`** 

```
├── Makefile          → Wrapper for sphinx-build command.
├── build             → Outputs.
├── make.bat          → Remnant of darker ages. 
└── source            → Text-sources, images, themes, customizations.
    ├── _static
    ├── _templates    → Themes.
    ├── conf.py       → Configuration, waiting for some shenanigans.
    └── index.rst     → Indexing and structuring device for this documentation.  
```

**`cat index.rst`**

```
.. Test Project documentation master file, created by
   sphinx-quickstart on Wed May 29 13:25:05 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Test Project's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
```

- `make` → Shows available targets.
- `make html` → Generates `build/html/`.

## Config shenanigans

### Adding Markdown support

- `pip install recommonmark`
- In `conf.py` add

```python
extensions = ['recommonmark']
source_suffix = ['.rst', '.md']
```

### Structurize project

`index.rst` drives the
 - start page
 - navigation
 - folder and file inclusion

We can mix static and globbed content 🎉 within sections

```
.. toctree::
   :hidden:
   :maxdepth: 1
   :caption: Start Here
   
   overview
   guide_login.md

.. toctree::
   :caption: Bare Metal
   :hidden:
   :maxdepth: 2
   :glob:

   hw_*
```

### Read the docs layout

- `pip install sphinx_rtd_theme`
- In `conf.py` add

```python
html_theme = 'sphinx_rtd_theme'
html_theme_options = {
    'display_version': False,
    'logo_only': False,
    'navigation_depth': 2,
    'collapse_navigation': True,
    'titles_only': False,
    'prev_next_buttons_location': None,
    #'style_nav_header_background': '#00FF00',
}
```
- [https://sphinx-rtd-theme.readthedocs.io/en/latest/configuring.html](https://sphinx-rtd-theme.readthedocs.io/en/latest/configuring.html)

## Conclusion & Outlook

- Sphinx rocks 👍.
- Markdown is an extension - ReSt and MD don't play well together 😩.
- Markdown, especially GfM, is not supported 😩.
- Certain features can be added (e.g. [sphinx-markdown-tables](https://github.com/ryanfox/sphinx-markdown-tables))
- Might be just right for certain documentation projects (but not all).
 - Alternative might be [pandoc](https://pandoc.org), [mkdocs](https://www.mkdocs.org), ...
- Initial thoughts and setup can be beneficial for various (sub)projects.
- Easy generation and deployment via continuous integration 🎉.

## Further information

- Templates can be installed globally or locally (templates-folder)
    - This empowers customization
- https://github.com/ryanfox/sphinx-markdown-tables
- https://github.com/yoloseem/awesome-sphinxdoc