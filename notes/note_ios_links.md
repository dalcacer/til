# Unsorted, chaotic iOS related bunch of links

- [Apple Developer The Basics](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/TheBasics.html#//apple_ref/doc/uid/TP40014097-CH5-ID309)
- [Apple Guides And Sample Code](https://developer.apple.com/library/content/navigation/)
- [Apple API Reference](https://developer.apple.com/reference/)
- [Apple WWDC Videos](https://developer.apple.com/videos/)

-  [How to Use iOS Charts API to Create Beautiful Charts in Swift](http://www.appcoda.com/ios-charts-api-tutorial/)
-  [Creating Charts and Graphs](https://developer.apple.com/library/content/documentation/AudioVideo/Conceptual/HTML-canvas-guide/CreatingChartsandGraphs/CreatingChartsandGraphs.html)
- [CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack)
- [Connect the UI to Code](https://developer.apple.com/library/content/referencelibrary/GettingStarted/DevelopiOSAppsSwift/Lesson3.html#//apple_ref/doc/uid/TP40015214-CH22-SW1)
- [Klassen- und Methodendeklaration in Swift](https://www.ralfebert.de/ios/swift-classes/)
-  [iOS 10-App entwickeln mit Swift und Xcode 8](https://www.ralfebert.de/ios/)
- [NSHipster Swift Documentation](http://nshipster.com/swift-documentation/)
-  [Swift Code Metrics Taylor](https://github.com/yopeso/Taylor)
-  [jazzy Documentation for Swift and Objective-C](https://github.com/realm/jazzy)
-  [cocoapods](http://cocoapods.org)
- [Plug-in Architectures](https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/LoadingCode/Concepts/Plugins.html)

## Swiftmonthly.com 10/16

-  [Article Declarative API Design in Swift](http://blog.benjamin-encz.de/post/declarative-api-design-in-swift/)
- [Video Tutorial Swift 3 Bottom Up: Ep1](https://www.youtube.com/watch?v=YFVa8p-5Eds)
-  [Book Swift Apprentice](https://www.amazon.co.uk/gp/product/1942878133/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=1634&creative=6738&creativeASIN=1942878133&linkCode=as2&tag=iosblog0b-21)


## Buildsystems and Tools

- https://developer.apple.com/videos/play/wwdc2015/406/

## Further Links

- [Apple Developer The Basics](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/TheBasics.html#//apple_ref/doc/uid/TP40014097-CH5-ID309)
- [Video Tutorial Swift 3 Bottom Up: Ep1 - Constants, Variables, Types and Comments.](https://www.youtube.com/watch?v=YFVa8p-5Eds)
- [Video Tutorial Swift 3 Bottom Up: Ep2 - Math and Boolean Operators](https://www.youtube.com/watch?v=MANAkWA4_f0)
- [Video Tutorial Swift 3 Bottom Up: Ep3 - Control Flow and Optionals](https://www.youtube.com/watch?v=Fwdu_RRqejk)
- [Apple Guides And Sample Code](https://developer.apple.com/library/content/navigation/)

- [https://github.com/futurice/ios-good-practices/blob/master/README.md](https://github.com/futurice/ios-good-practices/blob/master/README.md)
- [https://themindstudios.com/blog/mvp-vs-mvc-vs-mvvm-vs-viper/](https://themindstudios.com/blog/mvp-vs-mvc-vs-mvvm-vs-viper/)
- [https://hackernoon.com/mvvm-in-ios-revisited-1e2de1419ba6](https://hackernoon.com/mvvm-in-ios-revisited-1e2de1419ba6)
- [https://www.ryanipete.com/blog/ios/swift/objective-c/uidebugginginformationoverlay/](https://www.ryanipete.com/blog/ios/swift/objective-c/uidebugginginformationoverlay/)