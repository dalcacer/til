# Ansible

> [Ansible](https://ansible.com) is an open-source IT automation engine, which can remove drudgery from your work life, and will also dramatically improve the scalability, consistency, and reliability of your IT environment. 

- uses SSH to push commands
- no-client software (agent) needed
- yaml based environment description (playbooks)

![](images/guide_ansible_1.png)

- source sysadmincasts.com

## Installation

- `pip install ansible`

## Usage

- curate `/etc/ansible/hosts` - [inventory on ansible.com](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)
- make sure you can access each and every host via SSH
- perform ad-hoc commands - [ad-hoc commands on ansible.com](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html)
    - `ansible -m ping all`
    - `ansible all -a "/bin/uname -a"`
    - `ansible atlanta -a "/sbin/reboot" -f 10`
    - `ansible atlanta -m copy -a "src=/etc/hosts dest=/tmp/hosts"`
- curate playbooks

## Example playbook

```yaml
# basic setup for all our containers
---
- hosts: container
  become: true
  tasks:
  - include_tasks: ../ubuntu/basics.yml
  - include_tasks: ../ubuntu/monit.yml
  - include_tasks: ../ubuntu/certificates.yml
  - include_tasks: ../ubuntu/update_packages.yml
```

## Example task

```yaml
---
- name: set timezone to Europe/Berlin
  timezone:
    name: Europe/Berlin
- name: Install basic Applications
  apt:
    name: ['git', vim', 'curl', 'tree', 'htop', 'pv', 'stress', 'wget', 'build-essential', 'ruby', 'rsync', 'ufw', 'mosh', 'unattended-upgrades']
    state: latest
- name: ufw allow ssh
  command: ufw allow proto udp from 192.168.0.0/24 to any port 22
- name: ufw allow mosh
  command: ufw allow proto udp from 192.168.0.0/24 to any port 60001
- name: Generate locales (EN)
  command: locale-gen en_US.UTF-8
- name: Generate locales (DE)
  command: locale-gen de_DE.UTF-8
- name: Setup unattended-upgrades
  command: dpkg-reconfigure -plow unattended-upgrades --default-priority
- name: Copy unattended-upgrade settings to client
  copy: src=../files/unattended-upgrades/10periodic dest=/etc/apt/apt.conf.d/10periodic owner=root group=root mode=0644

```