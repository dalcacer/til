Notes and Howtos
===============

Once in a while I learn new stuff and take notes or write howtos based on these insights.

This page is part of `my til repository on gitlab <https://gitlab.com/dalcacer/til>`_ where I collect notes, self-written guides, and passed tutorials.

It somewhat reflects what I've been dealing with.

* as employee and student of the `university of applied sciences Osnabrück <https://www.hs-osnabrueck.de/>`_
* as employee of the `HörSys GmbH <https://hoersys.de>`_
* as employee of the `OtoJig GmbH <https://otojig.com>`_
* and as normal human being that uses its spare time to deepen insights in programming, general software development and administration.

.. toctree::
    :caption: Howtos
    :maxdepth: 2
    :hidden:
    :glob:
    
    guide_*

.. toctree::
    :caption: Notes
    :maxdepth: 2
    :hidden:
    :glob:
   
    note_*

