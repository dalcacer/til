## Clean Code

> We are authors. And one thing about authors is that they have readers. Indeed, authors are *responsible* for communicating well with their readers.`

> Leave the campground cleaner than you found it.

## Meaningful Names

- **Intention-Revealing Name**
  - `elapsedTimeInDays` , `daysSinceCreation`, ...
- **Avoid Disinformation**
  - `accountList` only if a actual List is used, if not `accountGroup`, `bunchOfAccounts`, `accounts`
  - Ambiguity `XYZControllerForEfficientHandlingOfStrings` vs. `XYZControllerForEfficientStorageOfStrings` . Choose more distinguishable names.
  - `Spelling similar concepts similarly is *information*. Using inconsistent spellings is *dis- information*. `
- **Make Meaningful Distinctions**
  - `If names must be different, then they should also mean something different.`
  - Noise: `Imagine that you have a Product class. If you have another called ProductInfo or ProductData` sematnically to close to each other. 
- **Use Pronounceable Names**
  - `genymdhms (generation date, year, month, day, hour, minute, and second)` vs generationTimestamp`
- **Use Searchable Names**
  - No `Single-letter names and numeric constants`
  - `MAX_CLASSES_PER_STUDENT` instead of 7
  - `e` not searchable
  - `My personal preference is that single-letter names can ONLY be used as local variables inside short methods`
- **Avoid encodings**
  - Hungarian Notation: we got strongly typed Objects, no need to type our Variablenames.
  - Member Prefixes
  - Interfaces and Implementations: No i-Notation. `ShapeFactory` and `ShapeFactoryImp`
- **Avoid Mental Mapping**
  - Write explicit variable names, so others do not have to wrap their head around the trickyness of the code.
- **Class Names**: Noun. `Customer`, `WikiPage`, `Account`, `AddressParser` not `Manager`, `Processor`, `Data` and `Info`
- **Method Names:** Verb. `postPayment`, `deletePage`, `save`. (`set`, `get`, `is` according to Standard). 
- **Don't be cute**: `HolyHandGrenade` vs. `DeleteItems`, `whack()` vs. `kill()`, `eatMyShorts()` vs. `abort()`.
- **Pick One Word per Concept**: `fetch`, `retreive`, `get` - chose one. `Controller`, `Driver`, `Manager`. **ℹ️ Setup a glossary or lexicon accompanying your development**.
- **Don't Pun** 
- **Use Solution Domain Names**: e.g. well-known patterns
- **Use Problem Domain Names**: Separating solution and problem domain concepts is part of the job of a good pro- grammer and designer.
- **Add Meaningful Context**
- **Don’t Add Gratuitous Context**: e.g. prefixing with `GSD` which yield a lot of `GSD` classes.

>  The hardest thing about choosing good names is that it requires good descriptive skills and a **shared cultural background**.

## Functions

- **Small!** ~ 20 lines

- **Blocks and Indenting**

- **Do one Thing**

  - > Functions should do one thing. They should do it well. THe should do it only.
  - > Functions that do one thing cannot be reasonably divided into sections.
  - Method extractions

  - Renaming

- One Level of Abstraction per Function

  - > Mixing levels of abstraction within a function is always confusing

- The stepdown rule

- Switch Statements

  - we *can* make sure that each switch statement is buried in a low-level class and is never repeated. We do this, of course, with polymorphism.

  - > My general rule for switch statements is that they can be tolerated if they appear only once, are used to create polymorphic objects, and are hidden behind an inheritance.

- Use Descriptive Names

- Function Arguments - as few as possible (niladic, monadic, dyadic, triadic, polyadic)

  - Common Monadic Forms
    - Function transforms argument
    - Function checks argument for property
  - Flag Arguments (are ugly) - Use n functions instead.
  - Dyadic Functions
  - Triads
  - Argument Objects: when more than three arguments needed condider Objects.
  - Argument Lists: 
  - Verbs and Keywords:

**Note:** Unclear parameter ordering is no problem with Obj-C and Swift. 

- Have No Side effects

