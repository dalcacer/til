# supabase

As explained by

- https://supabase.com
- [Superbase in 6 Minutes](https://www.youtube.com/watch?v=ogEitL8RwtQ)Local


- [supabase with react](https://supabase.com/docs/guides/with-react)


Supabase provides

- database: postgres
- auth: row level security
- edge functions: server-side typescript functions
- file storage
- auto-generated APIs
- observability

## With React

- `npm install @supabase/supabase-js`

```js
import { createClient } from '@supabase/supabase-js'
// Create a single supabase client for interacting with your database
const supabase = createClient('https://xyzcompany.supabase.co', 'public-anon-key')
```

```js
  const [posts, setPosts] = useState([])
  const [post, setPost] = useState({title: "", content: ""})
  const {title,content} = post

  async function fetchPosts(){
    const {data} =  await supabase.from('posts').select()
    setPosts(data)
    console.log("data: ", data)
  }

  async function createPost() {
    await supabase.from('posts').insert(
      [{title, content}]
    ).single()
    setPost({title: "", content: ""})
    fetchPosts()
  }

  // on first render
  useEffect(() => {
    fetchPosts()
  }, []);

```