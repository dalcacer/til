# BTRFS
 
- `apt install btrfs-tools`

As provided by [marksei](https://www.marksei.com/getting-started-with-btrfs/)

- `mkfs.btrfs /dev/sda` make a volume.
- `btrfs-convert /dev/sda`
- `mkfs.btrfs -f -L btrfspool /dev/sdd1 /dev/sdd2 /dev/sdd3` make a pool, `-f`  overwrite existing filesystem.
- Adding and deleting drives from pool
    - `btrfs device add /dev/sdX /mnt`
    - `btrfs device delete /dev/sdX /mnt`
