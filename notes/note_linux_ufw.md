# UFW - Uncomplicated Firewall 

ufw uncomplicated firewall is an easy frontend for iptables.

## Installation

- `apt install -y ufw`

## Firewall start/stop

- `ufw enable`
- `ufw disable`

## Adding rules

- `ufw allow|deny|reject SERVICE`
    - `ufw allow ssh`

- `ufw allow|deny [proto <protokoll>] [from <adresse> [port <port>]] [to <addresse> [port <port>]]`
    - `ufw allow proto tcp from any to 123.456.78.90 port 22`
    - `ufw allow 80/tcp`

- Block connections to a network interface:
    - `ufw deny in on eth0 from 15.15.15.51` [^digoc]

- Allow incoming SSH from specific IP address or subnet:
    - `ufw allow from 15.15.15.0/24 to any port 22`

## Add default rules

- `ufw default allow`      
- `ufw default deny`

## List existing rules

- `ufw status`
- `ufw status numbered`

## Delete existing rules

- `ufw delete no.` -- after `ufw status numbered` was issued
- `ufw delete allow 80/tcp`

## Source

[^digoc]: [UFW essentials at digitalocean.com](https://www.digitalocean.com/community/tutorials/ufw-essentials-common-firewall-rules-and-commands)