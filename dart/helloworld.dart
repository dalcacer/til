// this is single line comment  
  
/* This is a   
   Multi-line comment  
*/ 


class TestClass {   
   void disp() {     
      print("Hello World"); 
   } 
}  

var name = 'Smith';
final name1 = "Test";
const name2 = "Test";

void main() {
  TestClass c = new TestClass();   
  c.disp();
  print(name);
  print(name1);
  print(name2);

  const pi = 3.14; 
  const area = pi*12*12; 
  print("The output is ${area}"); 

   var a = 10; 
   var res = a > 12 ? "value greater than 10":"value lesser than or equal to 10"; 
   print(res); 
}
