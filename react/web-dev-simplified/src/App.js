import TodoList from './TodoList'
import React, {useState, useRef, useEffect} from 'react'
import { v4 as uuidv4 } from 'uuid';

const LOCAL_STORAGE_KEY = 'todoApp.todos'
function App() {
  const [todos, setTodos] = useState([
    //{id: 1, name: "Todo 1", complete: true},
    //{id: 2, name: "Todo 2", complete: false},
  ])

  const todoNameRef = useRef()
  
  // Runs on every render
  // useEffect(() => {
  //
  // });

  // Runs only on the first render
  // useEffect(() => {
  // }, []);

  useEffect(()=> {
    const storedTodos = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY))
    console.log(storedTodos)
    if (storedTodos) setTodos(storedTodos)
  }, [])
  

  useEffect(()=> {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(todos))
  }, [todos])

  
  function handleAddToDo(e) {
    const name = todoNameRef.current.value
    if (name == "") return
    console.log(name)
    setTodos(prevTodos => {
      return [... prevTodos, {id: uuidv4(), name: name, complete: false}
      ]
    })
    todoNameRef.current.value = null
  }

  function handleClearComplete(e) {
    const newTodos = todos.filter(todo => !todo.complete)
    setTodos(newTodos)
  }
  function toggleTodo(id) {
    const newTodos = [... todos]
    const todo = newTodos.find(todo => todo.id === id)
    todo.complete = !todo.complete
    setTodos(todos)
  }

  return (
    <>
      <TodoList todos={todos} toggleTodo={toggleTodo}/>
      <input ref={todoNameRef} type="text"/>
      <button onClick={handleAddToDo}> Add Todo</button>
      <button onClick={handleClearComplete}>Clear Complete</button>
      <div>{todos.filter(todo => !todo.complete).length} left to do</div>
    </>
  );
}

export default App;
