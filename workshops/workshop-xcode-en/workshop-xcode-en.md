footer: © HörSys GmbH, 2019
slidenumbers: true

# [fit] A gentle instruction to XCode
**and iOS development as well**

---

# Contents

- Xcode - first contact
- basics & hands-on
- 3rd. party libraries
- summary
- outlook
- resources/literature

---

# [fit] Xcode - first contact

---

# Xcode - First Contact

- Xcode 8.3.3
- iOS 10.2 (simulators and platforms)
- accounts & provisioning profiles

---

# [fit] basics & hands-on

- a coarse orientation
- storyboards
    - basic usage
    - layouting
    - outlets and actions
    - segues [ˈsegwās]
    - bindings
- project structure

---

# [fit] a coarse orientation

---

# a coarse orientation

- create project
    - iOS Single View Application
    - "cup-of-coffee"

<br/>

![inline, left, 75%](img/Xcode4.png)
![inline, right, 75%](img/Xcode5.png)

---





