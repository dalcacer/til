footer: © HörSys GmbH • 2019
slidenumbers: true

# [fit] A git workshop
**for non-programmers**

![inline](img/git-logo.tiff)

Date: 2018-08

---

# content

- What is git?
- Installation & configuration
- First steps
 - Github Desktop App
 - Working locally
 - Working together
 - Branches
 - Tags
- Further information
- Source

---

![Fit](img/the-problem.tiff)

---

# What is git?

- Linus Torvalds, 2005 - [https://git-scm.com](https://git-scm.com)
- distributed version control system

![Inline, Left](img/central-vcs.tiff) ![Inline, Right](img/decentral-vcs.tiff)

- Git has over 150 commands 😳
- We don't need them all 😅

--- 

# Installation & configuration

-  git is a command line tool
 -[git-osx-installer](https://sourceforge.net/projects/git-osx-installer/) : https://sourceforge.net/projects/git-osx-installer/
 - [gitforwindows.org](https://gitforwindows.org)
 - There are different/better ways of installation (Ask or attend course for programmers)
- There are various graphical user interfaces
 - [Github Desktop App](https://desktop.github.com) (free) • [Sourcetree App](https://www.sourcetreeapp.com) (free)
 - [Git Tower](https://git-tower.com) ($$$) • [GitKraken](http://gitkraken.com) ($$$)
 
--- 

