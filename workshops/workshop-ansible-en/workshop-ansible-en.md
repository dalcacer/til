footer: HörSys GmbH • 2022
slidenumbers: true
autoscale: true
slide-transition: true
theme: Simple Lato
slide-transition: true

![](images/infrastructure.jpg)

# [fit] A Gentle Instruction to Ansible

![inline](images/ansible.png)

---

![](images/contents.jpg)

# Contents

- What is Ansible?
- An overview
- Conclusion and Outlook

---

# What is Ansible?

> "Ansible is an IT orchestration engine that automates configuration management, application deployment and many other IT needs." -- [ansible.com](https://ansible.com)

- Agent-less 🎉 → no clutter.
- `All you need is love` - and SSH.
- YAML based inventory of hosts.
- YAML based **playbooks** define systems.
- Lots of convenient [modules](https://docs.ansible.com/ansible/latest/modules/list_of_all_modules.html)

---

# What is Ansible?

![inline](images/guide_ansible.png)

http://sysadmincasts.com

---

![](images/handson.jpg)

#[fit] Hands on

---

# Configure our Management Node

- `pip install ansible` **or** `conda install -c conda-forge ansible`
- Global host or local configuration: `vim /etc/ansible/hosts` or `vim ./hosts`

```sh
# This is the default ansible 'hosts' file.
#
# It should live in /etc/ansible/hosts
#
#   - Comments begin with the '#' character
#   - Blank lines are ignored
#   - Groups of hosts are delimited by [header] elements
#   - You can enter hostnames or ip addresses
#   - A hostname/ip can be a member of multiple groups
[my_computers]
hostname.local
```
- ❗️ Do not forget to setup `~/.ssh/config`
- ❗️ Ensure connectivity via `ssh DEDICATED_USER@hostname.local`

---

# Configure our Management Node

- Global or local ansible configuration  [^1]
- `vim /etc/ansible/ansible.cfg`
- `vim ./ansible.cfg`

```sh
[...]
inventory      = ./hosts
[...]
private_key_file = ~/.ssh/maintenance
[...]
```

[^1]: 👀 [https://docs.ansible.com/ansible/latest/reference_appendices/config.html](https://docs.ansible.com/ansible/latest/reference_appendices/config.html)

---

# Run ad-hoc Commands

- `ansible -m ping all`
- `ansible all -a "/bin/uname -a"`
- On host groups
  - `ansible bare_metal_ubuntu -a "/bin/uname -a"`
  - `ansible container -a "/bin/uname -a"`
- Use module to show setup information [^2]
  - `ansible all -m setup`
  - `ansible iron.oto -m setup`

[^2]: 👀 [https://docs.ansible.com/ansible/latest/collections/ansible/builtin/setup_module.html](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/setup_module.html)

---

# Playbooks

![](images/playbooks.jpg)

- A script is called `playbook`[^3]
- It's build on modules and plugins[^4]
  - apt, command, copy, ...
- Playbooks are run using 
- `ansible-playbook playbookfile.yml -K`
  -`K` prompts the user for a sudo/root password
- Also `ansible-lint playboofile.yml` for verification and enhancement

[^3]: 👀 [https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html)

[^4]: 👀 [https://docs.ansible.com/ansible/latest/collections/all_plugins.html](https://docs.ansible.com/ansible/latest/collections/all_plugins.html)

---

# Playbooks

![](images/playbooks.jpg)


```yaml
---
- hosts: container
  become: true
  tasks:
  - name: set timezone to Europe/Berlin
  timezone:
    name: Europe/Berlin
  - name: install basic Applications
  apt:
    name: ['zsh', 'git', 'vim' [...] 'mosh', 'unattended-upgrades', 'prometheus-node-exporter']
    state: latest
  - name: ufw allow ssh
  command: ufw allow proto udp to any port 22
  [...]
  - name: copy unattended-upgrade settings to client
  copy: src=../files/unattended-upgrades/10periodic dest=/etc/apt/apt.conf.d/10periodic owner=root group=root mode=0644
```

---

![](images/playbooks.jpg)

- Tasks can be distributed over files

```yaml
---
- hosts: container
  become: true
  tasks:
  - include_tasks: ../linux/basics.yml
  - include_tasks: ../linux/packages.yml
```

- Run `ansible-playbook playbooks/containers/all.yml -K`

---

![](images/playbooks.jpg)

- Playbooks can contain variables
  - KVP
  - encrypted KVP file

```yaml
# basic setup for 
---
- hosts: monitoring.local
  become: true
  vars:
    # https://github.com/prometheus/prometheus/releases
    prometheus_version: 2.33.0
    # https://github.com/prometheus/blackbox_exporter/releases
    blackbox_version: 0.19.0
    # https://github.com/prometheus/alertmanager/releases/
    alertmanager_version: 0.23.0
  vars_files: secrets.yml
  tasks:
  [...]
  - include_tasks: ../linux/prometheus_exporter.yml
  - include_tasks: ../linux/prometheus.yml
  - include_tasks: ../linux/blackbox.yml
  - include_tasks: ../linux/alertmanager.yml
  - include_tasks: ../linux/grafana.yml
```

---

![](images/playbooks.jpg)

- Copy a host-specific file

```yaml
- name: copy prometheus-exporter to client
  copy: src=../files/prometheus/{{inventory_hostname_short}}.prometheus-exporter.py dest=/opt/exporter.py owner=root group=root mode=0644
```

- Run tasks conditionally

```yaml
- name: copy lxcbackup script
  copy: src=../files/bin/hoerfoce-one.lxcbackup dest=/usr/bin/lxcbackup owner=root group=root mode=0776
  when: ansible_hostname == 'hoerforce-one'
```

---

![](images/playbooks.jpg)

- Download a new software version

```yaml
- name: download prometheus
  get_url:
    url: "https://github.com/prometheus/prometheus/releases/download/v{{ prometheus_version}}/prometheus-{{ prometheus_version }}.linux-armv7.tar.gz"
    dest: /tmp/
```

- Fill in passwords form a secure vault

```yaml
- name: insert smtp password
  command: sed -i 's/HELLO_SMTP_PASSWORD/{{ hello_smtp_password }}/g' /opt/alertmanager/alertmanager.yml
```

---

![](images/vault.jpg)

# Ansible Vault[^5]

- Password encrypted file that holds KVP
  - `ansible-vault [-h] [--version] [-v] {create,decrypt,edit,view,encrypt,encrypt_string,rekey} ...`
  - `ansible-vault edit playbooks/metal/secrets.yml`
- Needs to be unlocked for playbook run
  - `ansible-playbook playbooks/metal/watchberry.yml -K --ask-vault-pass`

[^5]: 👀 [https://docs.ansible.com/ansible/latest/cli/ansible-vault.html](https://docs.ansible.com/ansible/latest/cli/ansible-vault.html)

---

![](images/outlook.jpg)

# Conclusion & Outlook

- Ansible simply rocks 👍
- It documents implicit knowledge → can be complemented with Sphinx
- It makes reproduction and updating of systems easier (to some degree)
- cronjob automation is an option [^6]
- https://gitlab.hoersys.de/hoersys/infra/-/blob/master/doc/overview.md

[^6]: 👀 [https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html#ansible-pull](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html#ansible-pull)

---

# Credits

- apple-device-electronics-574089.jpg → Photo by Lukas from Pexels
- art-colorful-contemporary-2047905.jpg → Photo by Junior Teixeira from Pexels
- apple-coffee-coffee-shop-7101.jpg → Photo by Startup Stock Photos from Pexels
- blur-bottle-chemistry-248152.jpg → Photo by Pixabay from Pexels
- asphalt-car-close-up-861233.jpg → Photo by Oleg Magni from Pexels
- binoculars-blur-focus-63901.jpg Photo by Skitterphoto from Pexels

