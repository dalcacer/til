import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return PlatformApp(home: HomeScreen());
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(items: [
        BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.book_solid), label: "Articles"),
        BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.eye_solid), label: "Views"),
      ]),
      tabBuilder: (context, i) {
        return CupertinoTabView(
          builder: (context) {
            return CupertinoPageScaffold(
              navigationBar: CupertinoNavigationBar(
                middle: (i == 0) ? Text("Articles") : Text("Views"),
              ),
              child: Center(
                child: CupertinoButton(
                  child: Text(
                    "This is tab #$i",
                    style: CupertinoTheme.of(context)
                        .textTheme
                        .actionTextStyle
                        .copyWith(fontSize: 32),
                  ),
                  onPressed: () {
                    Navigator.of(context).push(
                      CupertinoPageRoute(builder: (context) {
                        return DetailScreen(i == 0 ? "Article" : "Views");
                      }),
                    );
                  },
                ),
              ),
            );
          },
        );
      },
    );
  }
}

class DetailScreen extends StatefulWidget {
  const DetailScreen(this.topic);

  final String topic;

  @override
  State<StatefulWidget> createState() {
    return new DetailScreenState();
  }
}

class DetailScreenState extends State<DetailScreen> {
  bool switchValue = false;

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(middle: Text("Details")),
        child: Center(
            child: Padding(
          padding: const EdgeInsets.all(32.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              CupertinoButton(
                  child: Text("Launch Action Sheet"),
                  onPressed: () {
                    showCupertinoModalPopup<int>(
                        context: context,
                        builder: (context) {
                          return CupertinoActionSheet(
                            title: Text("Some Choices"),
                            actions: [
                              CupertinoActionSheetAction(
                                onPressed: () {
                                  Navigator.pop(context, 1);
                                },
                                child: Text("one"),
                                isDefaultAction: true,
                              ),
                              CupertinoActionSheetAction(
                                onPressed: () {
                                  Navigator.pop(context, 2);
                                },
                                child: Text("two"),
                              )
                            ],
                          );
                        });
                  })
            ],
          ),
        )));
  }
}

// class DetailScreenState extends State<DetailScreen>{
//
//   bool switchValue= false;
//   @override
//   Widget build(BuildContext context) {
//     return CupertinoPageScaffold(
//         navigationBar: CupertinoNavigationBar(middle: Text("Details")),
//         child: Center(
//             child: Padding(
//                 padding: const EdgeInsets.all(32.0),
//                 child: Column(
//                   mainAxisSize: MainAxisSize.min,
//                   children: [
//                     Row(
//                       children: [
//                         Expanded(
//                           child: Text("A switch"),
//                         ),
//                         CupertinoSwitch(
//                             value: switchValue,
//                             onChanged: (value){
//                               setState(() => switchValue = value);
//                             })
//                       ],
//                     )
//                   ],
//                 )
//             )
//         )
//     );
//   }
// }
//   return CupertinoPageScaffold(
//       child: Center(
//           child: Text("Hello World",
//               style: CupertinoTheme.of(context)
//                   .textTheme
//                   .navLargeTitleTextStyle)));
// }
