# til - Today I Learned

A repository where I collect self-written guides, and passed tutorials.
It somewhat reflects what I've been dealing with.

- as employee and student of the [university of applied sciences Osnabrück](https://www.hs-osnabrueck.de/)
- as employee of the [HörSys GmbH](https://hoersys.de)
- as employee of the [OtoJig GmbH](https://otojig.com)
- and as normal human being that uses its spare time to deepen insights in programming and administration.