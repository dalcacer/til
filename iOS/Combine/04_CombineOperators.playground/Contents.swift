// As explained by [Sebastian Boldt](https://medium.com/ios-os-x-development/learn-master-%EF%B8%8F-the-basics-of-combine-in-5-minutes-639421268219)
// and [Vadim Bulavin](https://www.vadimbulavin.com/map-flatmap-switchtolatest-in-combine-framework/).
import Foundation
import Combine

// Transform elements.
print("map")
[1,2,3,4].publisher.map {
    return $0 * 10
}.sink { value in
    print(value)
}

// Aggregate values just like reduce
print("scan")
[1,2,3,4,5].publisher.scan(0) { seed, value in
    return seed + value
}.sink { value in
    print(value)
}

// Filter elements.

print("filter")
[2,30,22,5,60,1].publisher.filter{
    $0 > 10
}.sink { value in
    print(value)
}

// Transform elements.
// [Vadim Bulavin](https://www.vadimbulavin.com/map-flatmap-switchtolatest-in-combine-framework/)
print("flatMap")
struct User {
   let name: CurrentValueSubject<String, Never>
}

let userSubject = PassthroughSubject<User, Never>()
        
userSubject
.flatMap { $0.name }
.sink { print($0) }

let user = User(name: .init("User 1"))
userSubject.send(user)
