// As explained by [Brian Advent](https://www.youtube.com/watch?v=RysM_XPNMTw)

import Foundation
import Combine

enum WeatherError: Error{
  case thingsJustHappen(error: Error)
}
//let weatherPublisher = PassthroughSubject<Int, Never>()
//let weatherPublisher = PassthroughSubject<Void, Never>()
//let weatherPublisher = PassthroughSubject<Int, Error>()
let weatherPublisher = PassthroughSubject<Int, Never>()

let subscriber = weatherPublisher
  .filter { $0 > 25}
  .sink(receiveValue: {value in print("A summer day of \(value) C")})


let anotherSubscriber = weatherPublisher.handleEvents(
  receiveSubscription: { subscription in
  print("New subscription \(subscription)")
  }, receiveOutput: { output in
  print("New Value: Output \(output)")
}, receiveCompletion:{ error in
    print("Subscription completed with potential error \(error)")
},
   receiveCancel: { print("Subscription cancelled")
}).sink(receiveValue: {value in print("Subscriber received value: \(value)")} )

weatherPublisher.send(10)
weatherPublisher.send(20)
weatherPublisher.send(26)
weatherPublisher.send(15)
weatherPublisher.send(30)
//weatherPublisher.send(completion: Subscribers.Completion<WeatherError>.failure(.thingsJustHappen))

