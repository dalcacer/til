// As explained by [Brian Advent](https://www.youtube.com/watch?v=RysM_XPNMTw)

import Foundation
import Combine

enum WeatherError: Error{
  case thingsJustHappen
}
//let weatherPublisher = PassthroughSubject<Int, Never>()
//let weatherPublisher = PassthroughSubject<Void, Never>()
//let weatherPublisher = PassthroughSubject<Int, Error>()
let weatherPublisher = PassthroughSubject<Int, Never>()

let subscriber = weatherPublisher
  .filter { $0 > 25}
  .sink(receiveValue: {value in print("A summer day of \(value) C")})

weatherPublisher.send(10)
weatherPublisher.send(20)
weatherPublisher.send(26)
weatherPublisher.send(15)
weatherPublisher.send(30)

