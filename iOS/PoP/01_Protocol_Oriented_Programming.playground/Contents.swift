//https://www.youtube.com/watch?v=P6zT1BLBgEI
// Shows protocol GameUnit and three implementations Peasant, Soilder and Knight.
import Foundation

protocol GameUnit{
    var id: String { get }
    var isAlive: Bool { get set }
    var healthLevel: Int { get set }
    mutating func hitTaken()
}

class Peasant : GameUnit{
    var id: String
    var isAlive = true
    var healthLevel: Int = 2
    
    init(name: String){
        id = name
    }
    
    func hitTaken() -> (){
        if healthLevel > 0{
            healthLevel -= 1
            if healthLevel == 0{
                isAlive = false
            }
        }
        else{
            isAlive = false
        }
        let state = isAlive ? String(repeating: "❤️", count: healthLevel) : "☠️"
        print("\(id): Argh! \(state)")
    }
}


var peasant  = Peasant(name: "Igor")
peasant.hitTaken()
peasant.hitTaken()


class Soilder : GameUnit{
    var id: String
    var isAlive = true
    var healthLevel: Int = 5
    
    init(name: String){
        id = name
    }
    
    func hitTaken() -> (){
        if healthLevel > 0{
            healthLevel -= 1
            if healthLevel == 0{
                isAlive = false
            }
        }
        else{
            isAlive = false
        }
        let state = isAlive ? String(repeating: "❤️", count: healthLevel) : "☠️"
        print("\(id): Argh! \(state)")
    }
}


var soilder  = Soilder(name: "Walter")
soilder.hitTaken()
soilder.hitTaken()

class Knight : GameUnit{
    var id: String
    var isAlive = true
    var healthLevel: Int = 10
    
    init(name: String){
        id = name
    }
    
    func hitTaken() -> (){
        if healthLevel > 0{
            healthLevel -= 1
            if healthLevel == 0{
                isAlive = false
            }
        }
        else{
            isAlive = false
        }
        let state = isAlive ? String(repeating: "❤️", count: healthLevel) : "☠️"
        print("\(id): Argh! \(state)")
    }
}

var knight = Knight(name: "Sir Lancelot")
knight.hitTaken()
knight.hitTaken()

