// https://theswiftpost.co/swift-4s-codable/

import UIKit

struct Person: Codable {
   let name: String
   let age: Int
   let friends: [Person]?
}


// Serialize

let person = Person(name: "Brad", age: 53, friends: nil)
// Sorry Brad
let encoder = JSONEncoder()
let jsonData = try encoder.encode(person)
let jsonString = String(data: jsonData, encoding: .utf8)! // {"name":"Brad","age":53}
print(jsonString)

// Deserialize
let decoder = JSONDecoder()
let decoded = try decoder.decode(Person.self, from: jsonData)
print(decoded)
