import Foundation
import Dispatch

let serialQueue = DispatchQueue(label: "test.queue.serial")
let concurrentQueue = DispatchQueue(label: "test.queue.concurrent", attributes: .concurrent)

let dispatchGroup = DispatchGroup()

dispatchGroup.enter(
)
serialQueue.async {
  Thread.sleep(forTimeInterval: 1)
  print("💚 Serial Queue")
  dispatchGroup.leave()
}

dispatchGroup.enter()

concurrentQueue.async {
  Thread.sleep(forTimeInterval: 2)
  print("🖤 Concurrent Queue")
  dispatchGroup.leave()
}

dispatchGroup.wait() //sync

print("❌ Blocked")

print("✅ All work done")
