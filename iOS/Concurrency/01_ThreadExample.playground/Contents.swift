
import UIKit
import Foundation

let redHearts = {
    for i in 0...100 {
        print("❤️", i)
    }
}

let blackHearts = {
    for i in 0...100 {
        print("🖤", i)
    }
}

let redThread = Thread(block: redHearts)

let blackThread = Thread(block: blackHearts)

redThread.start()
blackThread.start()
