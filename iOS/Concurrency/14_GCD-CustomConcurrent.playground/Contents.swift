import Foundation
import Dispatch

let concurrentQueue = DispatchQueue(label: "test.queue", attributes: .concurrent)

for i in 1...5 {
  
  concurrentQueue.async {
    print("❤️ async dispatch \(i) started at \(Thread.current).")
    Thread.sleep(forTimeInterval: 1)
    print("❤️ async dispatch \(i) done.")
  }
}

for i in 1...5 {
  
  concurrentQueue.sync {
    print("🖤 sync dispatch \(i) started at \(Thread.current).")
    Thread.sleep(forTimeInterval: 1)
    print("🖤 sync dispatch \(i) done.")
  }
}
