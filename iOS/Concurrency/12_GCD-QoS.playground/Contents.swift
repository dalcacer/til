import Foundation
import Dispatch

let mainQueue = DispatchQueue.main
let userInteractiveQueue = DispatchQueue.global(qos: .userInteractive)
let userInitiatedQueue = DispatchQueue.global(qos: .userInitiated)
let utilityQueue = DispatchQueue.global(qos: .utility)
let backgroundQueue = DispatchQueue.global(qos: .background)

for i in 1...1000 {
  
  backgroundQueue.async {
    print("❤️ background \(i)")
  }
  
  utilityQueue.async {
    print("💜 utility \(i)")
  }
  
  userInitiatedQueue.async {
    print("💙 user initiated \(i)")
  }
  
  userInteractiveQueue.async {
    print("💚 user interactive \(i)")
  }
  
  mainQueue.async {
    print("💛 main \(i)")
  }
}
