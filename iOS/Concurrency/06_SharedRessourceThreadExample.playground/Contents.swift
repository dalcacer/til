// Without racecondiction
import Foundation

var lock = NSLock() //mutal exclusion mutex
var sharedValue = "initial"

let updateValue = { (value: String) -> Void in
  lock.lock()
  print("🖌 Writing value \(value)")
  sharedValue = value
  Thread.sleep(forTimeInterval: 0.1)
  
  guard sharedValue == value else {
    print("❌ Unexpected value: expected \(value) got \(sharedValue)")
    return
  }
  print("Okay \(value)")
  lock.unlock()
}


let first = {
  for _ in 1...10 {
    updateValue("first")
  }
}

let second = {
  for _ in 1...10 {
    updateValue("second")
  }
}

var firstThread = Thread(block:first)
var secondThread = Thread(block:second)

firstThread.start()
secondThread.start()
