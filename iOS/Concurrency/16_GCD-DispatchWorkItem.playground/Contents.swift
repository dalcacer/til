import Foundation
import Dispatch

print("Start...")

let workItem = DispatchWorkItem {
  print("📢 This is the work item speaking.")
}
DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: workItem )

//Added

workItem.cancel()
print("Opps.")
