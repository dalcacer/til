import Foundation

let redHearts = {
    for i in 0...100 {
      if i > 5 {
        Thread.exit()
      }
      print("❤️", i)
    }
}

let blackHearts = {
    for i in 0...100 {
      if i > 10 {
        Thread.exit()
      }
        print("🖤", i)
    }
}

let redThread = Thread(block: redHearts)
redThread.qualityOfService = .background

let blackThread = Thread(block: blackHearts)
blackThread.qualityOfService = .userInteractive

redThread.start()
blackThread.start()
