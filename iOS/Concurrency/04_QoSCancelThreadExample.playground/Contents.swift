import Foundation

var redThread: Thread?
var blackThread: Thread?

let redHearts = {
    for i in 0...100 {
      if Thread.current.isCancelled {
          Thread.exit()
      }
      print("❤️", i)
    }
}

let blackHearts = {
    for i in 0...100 {
      if i == 50 {
        redThread?.cancel()
      }
        print("🖤", i)
    }
}

redThread = Thread(block: redHearts)
redThread!.qualityOfService = .background

blackThread = Thread(block: blackHearts)
blackThread!.qualityOfService = .userInteractive

redThread!.start()
blackThread!.start()
