import Foundation
import Dispatch

let serialQueue = DispatchQueue(label: "test.queue")

for i in 1...5 {
  
  serialQueue.async {
    print("❤️ async dispatch \(i) started at \(Thread.current).")
    Thread.sleep(forTimeInterval: 1)
    print("❤️ async dispatch \(i) done.")
  }
}

for i in 1...5 {
  
  serialQueue.sync {
    print("🖤 sync dispatch \(i) started at \(Thread.current).")
    Thread.sleep(forTimeInterval: 1)
    print("🖤 sync dispatch \(i) done.")
  }
}
