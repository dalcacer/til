import Foundation
import Dispatch

for i in 1...10 {
  DispatchQueue.global().sync {
    print("❤️ work dispatched as \(i) executed on Thread \(Thread.current)")
  }
}

/*
 * DO NOT
 DispatchQueue.main.sync {
   print("TEST")
 }
 */

