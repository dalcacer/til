import Foundation

let redHearts = {
    for i in 0...100 {
        print("❤️", i)
    }
}

let blackHearts = {
    for i in 0...100 {
        print("🖤", i)
    }
}

let redThread = Thread(block: redHearts)
redThread.qualityOfService = .background

let blackThread = Thread(block: blackHearts)
blackThread.qualityOfService = .userInteractive

redThread.start()
blackThread.start()
