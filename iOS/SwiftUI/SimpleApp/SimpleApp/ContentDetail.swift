// https://www.youtube.com/watch?v=Pfw7zWxchQc&list=PLY1P2_piiWEY50_dT-DUdWaV-84YyFF5_

import SwiftUI

struct ContentDetail: View {
  
  var codeName: String
  
  var body: some View {
    NavigationView{
    VStack {
      Image(codeName).clipShape(Circle())
      Text("Developer: \(codeName)").font(.title)
      }.navigationBarTitle(Text(codeName), displayMode: .inline)
    }
  }
}

struct ContentDetail_Previews: PreviewProvider {
  static var previews: some View {
    ContentDetail(codeName: "one")
  }
}
