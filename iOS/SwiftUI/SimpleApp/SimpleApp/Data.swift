// https://www.youtube.com/watch?v=Pfw7zWxchQc&list=PLY1P2_piiWEY50_dT-DUdWaV-84YyFF5_

import Foundation
import SwiftUI

let developers = [
  Developer(id:1001, codeName:"one"),
  Developer(id:1002, codeName:"two"),
  Developer(id:1003, codeName:"three"),
  Developer(id:1004, codeName:"four")
]

struct Developer : Identifiable {
  var id: Int
  var codeName: String
}
