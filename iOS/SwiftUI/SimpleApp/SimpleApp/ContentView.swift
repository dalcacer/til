// https://www.youtube.com/watch?v=Pfw7zWxchQc&list=PLY1P2_piiWEY50_dT-DUdWaV-84YyFF5_

import SwiftUI

struct ContentView: View {
  var body: some View {
    NavigationView {
      List(developers){ developer in
        NavigationLink(destination: ContentDetail(codeName: developer.codeName)) {
          ContentRow(codeName: developer.codeName)
        }
      }.navigationBarTitle(Text("Developers"), displayMode: .large)
    }
  }
}


struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
