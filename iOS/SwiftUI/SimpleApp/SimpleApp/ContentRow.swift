// https://www.youtube.com/watch?v=Pfw7zWxchQc&list=PLY1P2_piiWEY50_dT-DUdWaV-84YyFF5_

import SwiftUI

struct ContentRow: View {
  
  var codeName: String
  
  var body: some View {
    HStack {
      Image(codeName).clipShape( Circle())
      //Image("one").clipShape( RoundedRectangle(cornerRadius: 10))
      VStack(alignment: .leading) {
        Text("Codename: \(codeName)")
          .font(.title)
        Text("Another great developer")
      }
      Spacer()
    }.padding()
  }
}

struct ContentRow_Previews: PreviewProvider {
  static var previews: some View {
    ContentRow(codeName: "one:")
  }
}
