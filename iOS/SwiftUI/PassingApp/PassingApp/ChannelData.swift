//
//  ChannelData.swift
//  PassingApp
//
//  Created by Dorian Alcacer Labrador on 16.03.20.
//  Copyright © 2020 Dorian Alcacer Labrador. All rights reserved.
//

import Foundation

final class ChannelData: ObservableObject {
  
  @Published var channelName = "YouTube Channel"
  
}
