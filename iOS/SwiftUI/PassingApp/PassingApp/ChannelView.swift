//
//  ChannelView.swift
//  PassingApp
//
//  Created by Dorian Alcacer Labrador on 16.03.20.
//  Copyright © 2020 Dorian Alcacer Labrador. All rights reserved.
//

import SwiftUI

struct ChannelView: View {
  @Environment(\.presentationMode) var presentationMode:Binding<PresentationMode>
  
  
  @EnvironmentObject var channelData: ChannelData
  
  var body: some View {
      VStack{
        TextField("Channel Name", text: $channelData.channelName)
        Divider()
        Button(action: {
          self.presentationMode.wrappedValue.dismiss()
        }){
          Text("Dismiss this VC")
        }
        Spacer()
      }.padding().navigationBarTitle("Channel Data")
    }
}

struct ChannelView_Previews: PreviewProvider {
    static var previews: some View {
        ChannelView()
    }
}
