 //
//  VideoIdea.swift
//  PassingApp
//
//  Created by Dorian Alcacer Labrador on 16.03.20.
//  Copyright © 2020 Dorian Alcacer Labrador. All rights reserved.
//

import Combine

 final class VideoIdea: ObservableObject{
  
  @Published var title: String = ""
  @Published var contentIdea: String = ""
  
 }
