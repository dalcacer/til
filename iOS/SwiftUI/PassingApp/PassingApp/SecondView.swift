//
//  SecondView.swift
//  PassingApp
//
//  Created by Dorian Alcacer Labrador on 16.03.20.
//  Copyright © 2020 Dorian Alcacer Labrador. All rights reserved.
//

import SwiftUI

struct SecondView: View {
  @Environment(\.presentationMode) var presentationMode:Binding<PresentationMode>
  @Binding var videoTitle: String
  @Binding var videoContent: String
  @EnvironmentObject var channelData: ChannelData
  
  var body: some View {
    NavigationView{
      VStack{
        TextField("Video title", text: $videoTitle)
        TextField("Video content", text: $videoContent)
        Divider()
        Button(action: {
          self.presentationMode.wrappedValue.dismiss()
        }){
          Text("Dismiss this VC")
        }
        Spacer()
      }.padding()
      .navigationBarTitle("\(channelData.channelName) Video")
    }
  }
}

//struct SecondView_Previews: PreviewProvider {
 //   static var previews: some View {
  //     SecondView()
   // }
//}
