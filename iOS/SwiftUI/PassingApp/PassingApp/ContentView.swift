//
//  ContentView.swift
//  PassingApp
//
//  Created by Dorian Alcacer Labrador on 12.03.20.
//  Copyright © 2020 Dorian Alcacer Labrador. All rights reserved.
//

import SwiftUI


struct ContentView: View {
  
  @State private var showingSecondVC = false
  @ObservedObject var videoIdea = VideoIdea()
  @EnvironmentObject var channelData: ChannelData
  
    var body: some View {
      NavigationView{
        VStack(){
          Text(videoIdea.title).font(.title)
          Text(videoIdea.contentIdea).font(.subheadline)
          Divider()
          NavigationLink(destination: ChannelView()){
            Text("Add Channel")
          }
          Button(action: {
            self.showingSecondVC.toggle()
          }){
            Text("Add New Idea")
          }.sheet(isPresented: $showingSecondVC){
            SecondView(videoTitle: self.$videoIdea.title, videoContent: self.$videoIdea.contentIdea).environmentObject(self.channelData)
          }
          Spacer()
        }.padding().navigationBarTitle("\(channelData.channelName)")
      }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
