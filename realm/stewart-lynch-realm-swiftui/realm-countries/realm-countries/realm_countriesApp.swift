

import SwiftUI

@main
struct realm_countriesApp: App {

    // Launch erverytime the app launches
    let migrator = Migrator()
    
    var body: some Scene {
        WindowGroup {
            TabView{
                CountryListView()
                    .tabItem{
                        Label("Countries", systemImage: "list.dash")
                    }
                AllCitiesView()
                    .tabItem{
                        Label("Cities", systemImage: "list.dash")
                    }
            }
            .onAppear{
                print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.path)
                UserDefaults.standard.setValue(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable") // remove log chatter about the constraints
            }
        }
    }
}
