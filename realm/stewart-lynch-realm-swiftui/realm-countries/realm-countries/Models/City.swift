import Foundation
import RealmSwift


class City: Object, ObjectKeyIdentifiable{
    @Persisted(primaryKey: true) var id: ObjectId
    @Persisted var name: String
    // inverse relationship to parent
    @Persisted(originProperty: "cities") var country: LinkingObjects<Country>
    
    convenience init(name: String){
        self.init()
        self.name = name
    }
}
