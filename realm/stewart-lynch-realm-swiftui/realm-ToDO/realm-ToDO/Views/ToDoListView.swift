//
//  ContentView.swift
//  realm-ToDO
//
//  Created by Dorian Alcacer Labrador on 09.09.22.
//

import SwiftUI
import RealmSwift

struct ToDoListView: View {
    //@ObservedResults(ToDo.self, sortDescriptor: SortDescriptor(keyPath: "completed", ascending: true)) var toDos
    @ObservedResults(ToDo.self) var toDos
    @State private var name = ""
    @State private var searchFilter = ""
    @FocusState private var focus: Bool?
    var body: some View {
        NavigationView {
            VStack{
                HStack {
                    TextField("New ToDo", text: $name)
                        .focused($focus, equals: true)
                        .textFieldStyle(.roundedBorder)
                    Spacer()
                    Button {
                        let newTodo = ToDo(name: name)
                        $toDos.append(newTodo)
                        name = ""
                        focus = nil
                    } label :{
                        Image(systemName: "plus.circle.fill")
                    }.disabled(name.isEmpty)
                }
                .padding()
                List(){
                    //ForEach(toDos.sorted(byKeyPath: "completed")){ toDo in
                    // For now the wrong item gets deleted.
                    ForEach(toDos.sorted(by: [
                        SortDescriptor(keyPath: "completed"),
                        SortDescriptor(keyPath: "urgency", ascending: false),])
                    ){ toDo in
                        ToDoListRow(toDo: toDo)
                    }
                    .onDelete(perform: $toDos.remove)
                    .listRowSeparator(.hidden)
                }
                .listStyle(.plain)
                .searchable(text: $searchFilter,
                            collection: $toDos, keyPath: \.name){
                    ForEach(toDos){todo in
                        Text(todo.name)
                            .searchCompletion(todo.name)
                    }
                }
            }
            .animation(.default, value: toDos)
            .navigationTitle("Realm ToDos")
        }
    }
}

struct ToDoListView_Previews: PreviewProvider {
    static var previews: some View {
        ToDoListView()
    }
}
