//
//  realm_ToDOApp.swift
//  realm-ToDO
//
//  Created by Dorian Alcacer Labrador on 09.09.22.
//

import SwiftUI

@main
struct realm_ToDOApp: App {
    var body: some Scene {
        WindowGroup {
            ToDoListView()
                .onAppear{
                    print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.path)
                    UserDefaults.standard.setValue(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable") // remove log chatter about the constraints
                }
        }
    }
}
